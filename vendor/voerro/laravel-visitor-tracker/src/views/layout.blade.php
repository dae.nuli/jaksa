@extends($visitortrackerLayout)

@section($visitortrackerSectionContent)

    {{-- <h1>Statistics</h1> --}}
<div class="box">
  <div class="box-body">
    @yield('visitortracker_content')
</div>
</div>
@endsection


@section('head-script')
<link rel="stylesheet" property="stylesheet" href="/vendor/visitortracker/css/visitortracker.css">
@endsection