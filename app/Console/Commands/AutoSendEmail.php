<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\QueueKegiatan;
use Carbon\Carbon;
use App\Models\ReminderStatus;
use App\Models\Kegiatan;
use App\Notifications\SendReminder;
use App\Admin;
use Notification;

class AutoSendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto send notification email to admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin = Admin::find(1);
        $nowDate = Carbon::today();
        $qk = QueueKegiatan::orderBy('kegiatan_id')->where('status', 0)->get();
        if (count($qk)) {
            foreach ($qk as $key => $value) {
                if (!empty($value->created_at)) {
                    $kegiatan = Kegiatan::find($value->kegiatan_id);
                    $fromDate = Carbon::parse($value->created_at);
                    $endDate = Carbon::parse($value->kegiatan->jadwal_sosialisasi);
                    $diff = $fromDate->diffInDays($endDate);
                    $div = floor($diff / 2);
                    $middleDate = $endDate->copy()->subDays($div);


                    if ($diff >= 4 && $diff <= 6) {
                        if ($nowDate->eq($middleDate)) {

                            $days = $middleDate->copy()->diffInDays($endDate);

                            if (!$value->searchDate($value->id, $middleDate)) {
                                ReminderStatus::create([
                                    'queue_kegiatan_id' => $value->id,
                                    'send_date' => $middleDate
                                ]);
                                Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                        }
                        
                    } elseif ($diff >= 7 && $diff <= 30) {

                        $twoDateBefore = $endDate->copy()->subDays(2);
                        $diffTwoDate = $fromDate->diffInDays($twoDateBefore);
                        $divTwo = floor($diffTwoDate / 2);
                        $middleTwoDate = $twoDateBefore->copy()->subDays($divTwo);

                        if ($nowDate->eq($middleTwoDate)) {

                            $days = $middleTwoDate->copy()->diffInDays($endDate);

                            if (!$value->searchDate($value->id, $middleTwoDate)) {
                                ReminderStatus::create([
                                    'queue_kegiatan_id' => $value->id,
                                    'send_date' => $middleTwoDate
                                ]);
                                Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                        }

                        if ($nowDate->eq($twoDateBefore)) {

                            $days = $twoDateBefore->copy()->diffInDays($endDate);

                            if (!$value->searchDate($value->id, $twoDateBefore)) {
                                ReminderStatus::create([
                                    'queue_kegiatan_id' => $value->id,
                                    'send_date' => $twoDateBefore
                                ]);
                                Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                        }

                    } elseif ($diff >= 31) {
                        $tenDateBefore = $endDate->copy()->subDays(4);
                        $diffTenDate = $fromDate->diffInDays($endDate);
                        $divTen = (int) floor($diffTenDate / 10) - 1;


                        // $middleTenDate = $tenDateBefore->copy()->subDays($divTen);

                        for($i = 1; $i <= $divTen; $i++) {
                            
                            $middleTenDate = $fromDate->copy()->addDays($i*10);
                            $days = $middleTenDate->copy()->diffInDays($endDate);

                            if ($nowDate->eq($middleTenDate)) {
                                if (!$value->searchDate($value->id, $middleTenDate)) {
                                    ReminderStatus::create([
                                        'queue_kegiatan_id' => $value->id,
                                        'send_date' => $middleTenDate
                                    ]);
                                    Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                                }
                            }
                        }

                        if ($nowDate->eq($tenDateBefore)) {
                            if (!$value->searchDate($value->id, $tenDateBefore)) {
                                $days = $tenDateBefore->copy()->diffInDays($endDate);
                                ReminderStatus::create([
                                    'queue_kegiatan_id' => $value->id,
                                    'send_date' => $tenDateBefore
                                ]);
                                Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                        }
                    }
                }
            }
        }
    }
}
