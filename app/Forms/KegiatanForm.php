<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\User;

class KegiatanForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('user_id', 'select', [
                'choices' => User::where('status', 1)->pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'Instansi',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            // ->add('instansi_pemohon', 'text', [
            //     'attr' => ['data-validation' => 'required']
            // ])
            ->add('nama_kegiatan', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('jenis_pekerjaan', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('pagu_anggaran', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'number form-control'
                ]
            ])
            ->add('sumber_dana', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('bentuk_pengawalan', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('jadwal_sosialisasi', 'text', [
                'value' => date('Y-m-d'),
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'datepicker form-control'
                ]
            ])
            ->add('nama_penanggungjawab', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('persentase_permasalahan', 'number', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('nomor_surat_perintah', 'text', [
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('penyerapan_anggaran', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('tim_pelaksana', 'text', [
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('tahapan_proyek', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('tahapan_pengawalan', 'text', [
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('mulai_spmk', 'text', [
                'value' => date('Y-m-d'),
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'datepicker form-control'
                ]
            ])
            ->add('berakhir_spmk', 'text', [
                'value' => date('Y-m-d'),
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'datepicker form-control'
                ]
            ])
            ->add('masa_pelaksanaan', 'text', [
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('jadwal_monitoring', 'text', [
                'value' => date('Y-m-d'),
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'datepicker form-control'
                ]
            ])
            ->add('phone', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            // ->add('email', 'text', [
            //     'attr' => ['data-validation' => 'required']
            // ])
            ->add('filekegiatan', 'file', [
                'template' => 'admin.kegiatan.files'
                // 'label' => 'File',
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('keterangan', 'text', [
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('status', 'choice', [
                'choices' => [3 => 'SEDANG PROSES', 1 => 'DITERIMA', 2 => 'DITOLAK', 4 => 'SELESAI', 0 => 'BARU'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [0],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}