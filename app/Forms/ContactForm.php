<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ContactForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('phone', 'text', [
                'label' => 'Nomor Telefon',
                'attr' => ['data-validation' => 'required']
            ])
            ->add('email', 'email', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('web', 'text', [
                'label' => 'Alamat Web',
                'attr' => ['data-validation' => 'required']
            ])
            ->add('facebook', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('whatsapp', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('instagram', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('google_plus', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('address', 'textarea', [
                'label' => 'Alamat Kantor',
                'attr' => ['data-validation' => 'required']
            ]);
    }
}