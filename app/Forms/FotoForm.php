<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class FotoForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Judul',
                'attr' => ['data-validation' => 'required']
            ])
            ->add('content', 'textarea', [
                'label' => 'Deskripsi',
                'attr' => ['data-validation' => 'required']
            ])
            // ->add('foto', 'file', [
            //     'attr' => ['data-validation' => 'required']
            // ])
            ->add('foto', 'file', [
                'template' => 'admin.foto.create_file'
                // 'label' => 'File',
                // 'attr' => ['data-validation' => 'required']
            ])
            ->add('status', 'choice', [
                'choices' => [1 => 'ACTIVE', 0 => 'NOT ACTIVE'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [1],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}