<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\User;

class PengaduanForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('user_id', 'select', [
                'choices' => User::where('status', 1)->pluck('name', 'id')->toArray(),
                'empty_value' => '- Please Select -',
                'label' => 'User',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control'
                ]
            ])
            ->add('nama_pengadu', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('nama_kegiatan', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('penanggungjawab', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('phone', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('filepengaduan', 'file', [
                'template' => 'admin.pengaduan.files'
            ])
            ->add('permasalahan', 'textarea', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('keterangan', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('status', 'choice', [
                'choices' => [0 => 'BARU', 1 => 'DITERIMA', 2 => 'DITOLAK', 3 => 'SEDANG PROSES', 4 => 'SELESAI'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [0],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}