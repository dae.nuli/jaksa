<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('email', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('password', 'password', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('status', 'choice', [
                'choices' => [1 => 'ACTIVE', 2 => 'NOT ACTIVE', 0 => 'NEW'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [0],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}