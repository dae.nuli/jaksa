<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class UUForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Judul',
                'attr' => ['data-validation' => 'required']
            ])
            ->add('content', 'textarea', [
                'label' => 'Content',
                'attr' => [
                    'data-validation' => 'required',
                    'id' => 'textarea'
                ]
            ])
            ->add('uu', 'file', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('status', 'choice', [
                'choices' => [1 => 'ACTIVE', 0 => 'NOT ACTIVE'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [0],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}