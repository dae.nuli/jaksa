<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ProfileForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('email', 'text', [
                'attr' => ['data-validation' => 'required', 'disabled' => '']
            ])
            ->add('password', 'password', [
                'value' => ''
            ])
            ->add('password_confirmation', 'password');
    }
}