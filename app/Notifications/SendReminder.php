<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Kegiatan;
use App\Admin;
use Carbon\Carbon;

class SendReminder extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Kegiatan $kegiatan, $day)
    {
        // $this->admin = $admin;
        $this->kegiatan = $kegiatan;
        $this->day = $day;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line('Thank you for using our application!');
        // $user = $this->admin;
        $kegiatan = $this->kegiatan;

        return (new MailMessage)
            // ->from(env('MAIL_FROM_ADDRESS'))
            ->subject('Reminder Permohonan')
            ->greeting(sprintf('Hello Administrator'))
            ->line('Nama kegiatan '.$kegiatan->nama_kegiatan.' akan melakukan sosialisasi '.$this->day.' hari yang akan datang, pada tanggal ('.Carbon::parse($kegiatan->jadwal_sosialisasi)->format('d F Y').')')
            ->action('Silahkan cek', route('admin.kegiatan.show', $kegiatan->id))
            ->line('Thank you!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
