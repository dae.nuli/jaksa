<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Kegiatan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class InvoicesExport implements ShouldAutoSize, FromView
{
    // public function collection()
    // {
    //     return Kegiatan::all();
    // }

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
    	$data = $this->data;
    	return view('admin.kegiatan.download_excel', $data);
        // return view('admin.kegiatan.download_excel', [
        //     'invoices' => Kegiatan::all(),
        //     'month' => 'Jan',
        //     'year' => '2019'
        // ]);
    }

    // public function registerEvents(): array
    // {
        // return [
        //     BeforeExport::class  => function(BeforeExport $event) {
        //         $event->writer->getProperties()->setCreator('Patrick');
        //     // },
        //     // AfterSheet::class    => function(AfterSheet $event) {
        //         $event->writer->getProperties()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

        //         $event->writer->getProperties()->styleCells(
        //             'B2:G8',
        //             [
        //                 'borders' => [
        //                     'outline' => [
        //                         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
        //                         'color' => ['argb' => 'FFFF0000'],
        //                     ],
        //                 ]
        //             ]
        //         );
        //     },
            // Handle by a closure.
            // BeforeExport::class => function(BeforeExport $event) {
            //     $event->writer->getProperties()->setCreator('Patrick');
            // },
// $spreadsheet
            // Array callable, refering to a static method.
            // BeforeWriting::class => [self::class, 'beforeWriting'],

            // Using a class with an __invoke method.
            // BeforeSheet::class => new BeforeSheetHandler()
        // ];
    // }
}