<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Admin;

class Undang extends Model
{
    protected $table = 'undang_undang';

    protected $fillable = [
        'admin_id', 'title', 'content', 'file', 'status'
    ];

    public function admin()
    {
    	return $this->belongsTo(Admin::class);
    }

    public static function boot()
    {
        parent::boot();
        static::saving(function($model)
        {
            $model->admin_id = Auth::guard('admin')->user()->id;
            // do some logging
            // override some property like $model->something = transform($something);
        });
    }
}
