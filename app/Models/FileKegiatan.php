<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileKegiatan extends Model
{
    protected $table = 'file_kegiatan';

    protected $fillable = [
        'kegiatan_id', 'file'
    ];

}
