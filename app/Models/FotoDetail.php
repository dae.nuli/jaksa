<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Foto;

class FotoDetail extends Model
{
    protected $table = 'foto_detail';

    protected $fillable = [
        'foto_id', 'file'
    ];

    public function foto()
    {
        return $this->belongsTo(Foto::class);
    }

    // public function setAdminIdAttribute($value)
    // {
    // 	$this->attributes['admin_id'] = Auth::guard('admin')->user()->id;
    // }
}
