<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    protected $fillable = [
        'phone', 'email', 'web', 'facebook', 'whatsapp', 'instagram', 'google_plus', 'address'
    ];
}
