<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Admin;

class Video extends Model
{
    protected $table = 'video';

    protected $fillable = [
        'admin_id', 'title', 'content', 'file', 'status'
    ];

    public function admin()
    {
    	return $this->belongsTo(Admin::class);
    }

    public static function boot()
    {
        parent::boot();
        static::saving(function($model)
        {
            $model->admin_id = Auth::guard('admin')->user()->id;
            // do some logging
            // override some property like $model->something = transform($something);
        });
    }

    // public function getFileAttribute($value)
    // {
    //     return ''.$value;
    // }
}
