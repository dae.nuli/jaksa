<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Pengaduan extends Model
{
    protected $table = 'pengaduan';

    protected $fillable = [
		'user_id', 'nama_pengadu', 'nama_kegiatan', 'permasalahan', 'penanggungjawab', 
		'phone', 'keterangan', 'status'
	];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
