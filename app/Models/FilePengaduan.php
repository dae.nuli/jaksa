<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilePengaduan extends Model
{
    protected $table = 'file_pengaduan';

    protected $fillable = [
        'pengaduan_id', 'file'
    ];
}
