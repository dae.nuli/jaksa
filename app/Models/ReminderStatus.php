<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReminderStatus extends Model
{
    protected $table = 'reminder_status';

    protected $fillable = [
        'queue_kegiatan_id', 'send_date'
    ];
}
