<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Kegiatan extends Model
{
    protected $table = 'kegiatan';

    protected $fillable = [
        'user_id', 'nama_kegiatan', 'jenis_pekerjaan', 'pagu_anggaran', 'sumber_dana', 
        'jadwal_sosialisasi', 'nomor_surat_perintah', 'penyerapan_anggaran', 'tim_pelaksana', 'tahapan_proyek', 
        'tahapan_pengawalan', 'mulai_spmk', 'berakhir_spmk', 'masa_pelaksanaan', 'jadwal_monitoring', 'bentuk_pengawalan', 
        'persentase_permasalahan', 'nama_penanggungjawab', 'phone', 'penyerapan_anggaran', 'status', 'keterangan'
    ];

    public function setPaguAnggaranAttribute($nilai)
    {
        $this->attributes['pagu_anggaran'] = str_replace('.','',$nilai);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
