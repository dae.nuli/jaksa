<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminPermission extends Model
{
    protected $table = 'admin_permission';
    protected $primaryKey = null;
	public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'admin_id', 'permission_id'
    ];
}
