<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role;

class AdminRole extends Model
{
    protected $table = 'admin_role';
    protected $primaryKey = null;
	public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'admin_id', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
