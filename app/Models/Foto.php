<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Admin;
use App\Models\FotoDetail;
class Foto extends Model
{
    protected $table = 'foto';

    protected $fillable = [
        'admin_id', 'title', 'content', 'status'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public static function boot()
    {
        parent::boot();
        static::saving(function($model)
        {
            $model->admin_id = Auth::guard('admin')->user()->id;
            // do some logging
            // override some property like $model->something = transform($something);
        });
    }
    public function detail()
    {
        return $this->hasMany(FotoDetail::class, 'foto_id')->orderBy('id', 'desc');
    }

    public function oneFoto()
    {
        return $this->detail()->first();
    }
    // {
    // 	$this->attributes['admin_id'] = Auth::guard('admin')->user()->id;
    // }
}



