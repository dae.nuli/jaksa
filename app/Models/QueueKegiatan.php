<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ReminderStatus;
use App\Models\Kegiatan;

class QueueKegiatan extends Model
{
    protected $table = 'queue_kegiatan';

    protected $fillable = [
        'kegiatan_id', 'status', 'created_at'
    ];

    public function kegiatan()
    {
    	return $this->belongsTo(Kegiatan::class);
    }

    public function reminder()
    {
    	return $this->hasMany(ReminderStatus::class, 'queue_kegiatan_id');
    }

    public function searchDate($id, $date)
    {
    	return $this->reminder()
		    	->where('queue_kegiatan_id', $id)
		    	->where('send_date', $date)->count();
    }

}
