<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Undang;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;
use DataTables;
use Form;
use File;
use Str;

class UndangController extends Controller
{
    private $folder = 'admin.uu';
    private $uri = 'admin.uu';
    private $title = 'Undang';
    private $desc = 'Description';


    public function __construct(Undang $table) 
    {
        $this->middleware('permission:undang_index', ['only' => ['index','data']]);
        $this->middleware('permission:undang_detail', ['only' => ['detail']]);
        $this->middleware('permission:undang_create', ['only' => ['create','store']]);
        $this->middleware('permission:undang_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:undang_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->storeDir = base_path('public/store');
        $this->destinationDir = base_path('public/store/uu');
        $this->storageDir = 'store/uu';
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        // if ($request->ajax()) {
            $data = $this->table->select(['id', 'title', 'content', 'file', 'created_at', 'status']);
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('content', function ($index) {
                    return Str::words($index->content, 3);
                })
                ->editColumn('title', function ($index) {
                    return Str::words($index->title, 4);
                })
                ->editColumn('status', function ($index) {
                    if($index->status) {
                        return '<span class="label label-success">ACTIVE</span>';
                    } else {
                        return '<span class="label label-danger">NOT ACTIVE</span>';
                    }
                })
                ->editColumn('file', function ($index) {
                    if($index->file) {
                        return '<a href="'.url('admin/uu/view/'.$index->id).'" target="_blank" width="200" />'.Str::words($index->title, 4).'</a>';
                    } else {
                        return '-';
                    }
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('undang_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth('admin')->user()->can('undang_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    $tag .= (auth('admin')->user()->can('undang_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'file', 'action'])
                ->make(true);
        // }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\UUForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\UUForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('uu', 'file', [
            'attr' => ['data-validation' => '']
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $filename = $request->uu;
        if($request->hasFile('uu')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            $namaFile = md5($filename->getClientOriginalName() . time()).'.'.$filename->extension();
            // $namaFile = str_random(10).'.'.$filename->extension();
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }
            $filename->move($destination, $namaFile);

            $request->merge(['file' => $namaFile]);
        }else{
            unset($request['file']);
        }
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $filename = $request->uu;
        if($request->hasFile('uu')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            $namaFile = md5($filename->getClientOriginalName() . time()).'.'.$filename->extension();
            // $namaFile = str_random(10).'.'.$filename->extension();
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }
            $filename->move($destination, $namaFile);

            $item = $this->table->findOrFail($id);
            if(!empty($item->file)){
                if(File::isDirectory($destination)){
                    Storage::delete($this->storageDir.'/'.$item->file);
                }
            }

            $request->merge(['file' => $namaFile]);
        }else{
            unset($request['file']);
        }
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $destination = $this->destinationDir;
        if(!empty($tb->file)){
            if(File::isDirectory($destination)){
                Storage::delete($this->storageDir.'/'.$tb->file);
            }
        }
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $uu = $this->table->findOrFail($id);
        $data['uu'] = $uu;
        $data['form'] = $formBuilder->create('App\Forms\UUForm', [
            'method' => 'POST',
            'model' => $uu,
            // 'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('title', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('content', 'static', [
            'template' => 'admin.uu.attachment_detail',
            'value' => function ($val) use ($uu) {
                return $uu;
            }
        ])
        // ->modify('content', 'textarea', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        ->remove('uu')
        ->addAfter('content', 'file', 'static')->modify('file', 'static', [
            'template' => 'admin.uu.attachment',
            'value' => function ($val) use ($uu) {
                return $uu;
            }
        ])
        ->modify('status', 'choice', [
            'attr' => [
                'disabled' => '',
                'class' => 'disable'
            ]
        ]);
        // ->modify('file_attachment', 'static', [
        //     'template' => 'admin.topup.detail_attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ]);
        // $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.detail',$data);
    }

    public function viewFile($id)
    {
        $item = $this->table->findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }
}
