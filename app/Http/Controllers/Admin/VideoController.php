<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Video;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;
use DataTables;
use Form;
use File;
use Str;

class VideoController extends Controller
{
    private $folder = 'admin.video';
    private $uri = 'admin.video';
    private $title = 'Video';
    private $desc = 'Description';


    public function __construct(Video $table) 
    {
        $this->middleware('permission:video_index', ['only' => ['index','data']]);
        $this->middleware('permission:video_detail', ['only' => ['detail']]);
        $this->middleware('permission:video_create', ['only' => ['create','store']]);
        $this->middleware('permission:video_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:video_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->storeDir = base_path('public/store');
        $this->destinationDir = base_path('public/store/video');
        $this->storageDir = 'store/video';
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'title', 'file', 'content', 'created_at', 'status']);
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('file', function ($index) {
                    $url = 'https://www.youtube.com/watch?v='.$index->file;
                    return '<a href="'.$url.'" target="_blank">'.str_limit(preg_replace('#^https?://#', '', $url), 20).'</a>';
                })
                ->editColumn('content', function ($index) {
                    return Str::words($index->content, 3);
                })
                ->editColumn('status', function ($index) {
                    if($index->status) {
                        return '<span class="label label-success">ACTIVE</span>';
                    } else {
                        return '<span class="label label-danger">NOT ACTIVE</span>';
                    }
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('video_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth('admin')->user()->can('video_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    $tag .= (auth('admin')->user()->can('video_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'file', 'status', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\VideoForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\VideoForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('file', 'text', [
            'value' => function ($val) {
                return 'https://www.youtube.com/watch?v='.$val;
            } 
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        // $fileVideo = $request->video;
        // if($request->hasFile('video')) {
        //     $store = $this->storeDir;
        //     $destination = $this->destinationDir;
        //     $namaFile = md5($fileVideo->getClientOriginalName() . time()).'.'.$fileVideo->extension();
        //     // $namaFile = str_random(10).'.'.$fileVideo->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($destination)){
        //         File::makeDirectory($destination, 0777);
        //     }
        //     $fileVideo->move($destination, $namaFile);

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }
        $request->merge([
            'file' => explode('=', $request->file)[1]
        ]);
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        // $fileVideo = $request->video;
        // if($request->hasFile('video')) {
        //     $store = $this->storeDir;
        //     $destination = $this->destinationDir;
        //     $namaFile = md5($fileVideo->getClientOriginalName() . time()).'.'.$fileVideo->extension();
        //     // $namaFile = str_random(10).'.'.$fileVideo->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($destination)){
        //         File::makeDirectory($destination, 0777);
        //     }
        //     $fileVideo->move($destination, $namaFile);

        //     $item = $this->table->findOrFail($id);
        //     if(!empty($item->file)){
        //         if(File::isDirectory($destination)){
        //             Storage::delete($this->storageDir.'/'.$item->file);
        //         }
        //     }

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }
        $request->merge([
            'file' => explode('=', $request->file)[1]
        ]);
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        // $destination = $this->destinationDir;
        // if(!empty($tb->file)){
        //     if(File::isDirectory($destination)){
        //         Storage::delete($this->storageDir.'/'.$tb->file);
        //     }
        // }
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $video = $this->table->findOrFail($id);
        $data['video'] = $video;
        $data['form'] = $formBuilder->create('App\Forms\VideoForm', [
            'method' => 'POST',
            'model' => $video,
            // 'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('title', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('content', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('file', 'text', [
            'attr' => [
                'disabled' => '',
            ],
            'value' => function ($val) {
                return 'https://www.youtube.com/watch?v='.$val;
            } 
        ])
        // ->remove('video')
        // ->addAfter('content', 'file', 'static')->modify('file', 'static', [
        //     'template' => 'admin.video.attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ])
        ->modify('status', 'choice', [
            'attr' => [
                'disabled' => '',
                'class' => 'disable'
            ]
        ]);
        // ->modify('file_attachment', 'static', [
        //     'template' => 'admin.topup.detail_attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ]);
        // $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.detail',$data);
    }
}
