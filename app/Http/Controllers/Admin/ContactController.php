<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\Contact;

class ContactController extends Controller
{
    private $folder = 'admin.contact';
    private $uri = 'admin.contact';
    private $title = 'Contact';
    private $desc = 'Description';


    public function __construct(Contact $table) 
    {
        $this->middleware('permission:contact_index', ['only' => ['index','update']]);
        $this->table = $table;
    }

    public function index(FormBuilder $formBuilder)
    {
        $tbl = $this->table->find(1);
        $data['form'] = $formBuilder->create('App\Forms\ContactForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', 1)
        ]);

        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        return view($this->folder.'.index', $data);
    }

    public function update(Request $request, $id)
    {
    	$this->table->findOrFail($id)->update($request->all());
    	return redirect()->back()->with('contact', 'Data sudah diperbarui');
    }
}
