<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;
use DataTables;
use Form;
use File;
use Str;

class SliderController extends Controller
{
    private $folder = 'admin.slider';
    private $uri = 'admin.slider';
    private $title = 'Slider';
    private $desc = 'Description';


    public function __construct(Slider $table) 
    {
        $this->middleware('permission:slider_index', ['only' => ['index','data']]);
        $this->middleware('permission:slider_detail', ['only' => ['detail']]);
        $this->middleware('permission:slider_create', ['only' => ['create','store']]);
        $this->middleware('permission:slider_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:slider_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->storeDir = base_path('public/store');
        $this->destinationDir = base_path('public/store/slider');
        $this->storageDir = 'store/slider';
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'title', 'file', 'content', 'created_at', 'status']);
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('content', function ($index) {
                    return Str::words($index->content, 3);
                })
                ->editColumn('title', function ($index) {
                    return Str::words($index->title, 4);
                })
                ->editColumn('status', function ($index) {
                    if($index->status) {
                        return '<span class="label label-success">ACTIVE</span>';
                    } else {
                        return '<span class="label label-danger">NOT ACTIVE</span>';
                    }
                })
                ->editColumn('file', function ($index) {
                    if($index->file) {
                        return '<img src="'.asset($this->storageDir.'/'.$index->file).'" width="200" />';
                    } else {
                        return '-';
                    }
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('slider_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth('admin')->user()->can('slider_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    $tag .= (auth('admin')->user()->can('slider_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'file', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\SliderForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\SliderForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('attach', 'static', [
            'template' => 'admin.slider.edit_file',
            'value' => function ($val) use ($tbl) {
                return $tbl;
            }
        ])
        // ->modify('foto', 'file', [
        //     'attr' => ['data-validation' => '']
        // ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            $slide = $request->attach;

            $namaFile = md5($slide->getClientOriginalName() . time()).'.'.$slide->extension();

            $slide->move($destination, $namaFile);

            $request->merge(['file' => $namaFile]);

        } else {
            unset($request['file']);
        }

        $row = $this->table->create($request->all());

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            $item = $this->table->findOrFail($id);
            if(!empty($item->file)){
                if(File::isDirectory($destination)){
                    Storage::delete($this->storageDir.'/'.$item->file);
                }
            }

            $slide = $request->attach;
            $namaFile = md5($slide->getClientOriginalName() . time()).'.'.$slide->extension();

            $slide->move($destination, $namaFile);

            $request->merge(['file' => $namaFile]);

        } else {
            unset($request['file']);
        }

        $this->table->findOrFail($id)->update($request->all());

        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);

        $destination = $this->destinationDir;
        if(!empty($tb->file)){
            if(File::isDirectory($destination)){
                Storage::delete($this->storageDir.'/'.$tb->file);
            }
        }
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $slider = $this->table->findOrFail($id);

        $data['slider'] = $slider;
        $data['form'] = $formBuilder->create('App\Forms\SliderForm', [
            'method' => 'POST',
            'model' => $slider,
            // 'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('title', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('content', 'textarea', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('attach', 'static', [
            'template' => 'admin.slider.detail_file',
            'value' => function ($val) use ($slider) {
                return $slider;
            }
        ])
        // ->remove('foto')
        // ->addAfter('content', 'file', 'static')->modify('file', 'static', [
        //     'template' => 'admin.foto.attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ])
        ->modify('status', 'choice', [
            'attr' => [
                'disabled' => '',
                'class' => 'disable'
            ]
        ]);
        // ->modify('file_attachment', 'static', [
        //     'template' => 'admin.topup.detail_attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ]);
        // $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.detail',$data);
    }

    public function viewFile($id)
    {
        $item = $this->table->findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }
}
