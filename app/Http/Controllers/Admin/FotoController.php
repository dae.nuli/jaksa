<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Foto;
use App\Models\FotoDetail;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;
use DataTables;
use Form;
use File;
use Str;

class FotoController extends Controller
{
    private $folder = 'admin.foto';
    private $uri = 'admin.foto';
    private $title = 'Foto';
    private $desc = 'Description';


    public function __construct(Foto $table) 
    {
        $this->middleware('permission:foto_index', ['only' => ['index','data']]);
        $this->middleware('permission:foto_detail', ['only' => ['detail']]);
        $this->middleware('permission:foto_create', ['only' => ['create','store']]);
        $this->middleware('permission:foto_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:foto_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->storeDir = base_path('public/store');
        $this->destinationDir = base_path('public/store/foto');
        $this->storageDir = 'store/foto';
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'title', 'content', 'created_at', 'status']);
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('content', function ($index) {
                    return Str::words($index->content, 3);
                })
                ->editColumn('title', function ($index) {
                    return Str::words($index->title, 4);
                })
                ->editColumn('status', function ($index) {
                    if($index->status) {
                        return '<span class="label label-success">ACTIVE</span>';
                    } else {
                        return '<span class="label label-danger">NOT ACTIVE</span>';
                    }
                })
                // ->editColumn('file', function ($index) {
                //     if($index->file) {
                //         return '<img src="'.asset('store/foto/'.$index->file).'" width="200" />';
                //     } else {
                //         return '-';
                //     }
                // })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('foto_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth('admin')->user()->can('foto_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    $tag .= (auth('admin')->user()->can('foto_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'file', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\FotoForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $file = FotoDetail::where('foto_id', $id)
                ->orderBy('id', 'desc')
                ->pluck('file', 'id')->toArray();

        $data['form'] = $formBuilder->create('App\Forms\FotoForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('foto', 'static', [
            'template' => 'admin.foto.edit_file',
            'value' => function ($val) use ($file) {
                return $file;
            }
        ])
        // ->modify('foto', 'file', [
        //     'attr' => ['data-validation' => '']
        // ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $row = $this->table->create($request->all());

        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                FotoDetail::create([
                    'foto_id' => $row->id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }

        // $fileFoto = $request->foto;
        // if($request->hasFile('foto')) {
        //     $store = $this->storeDir;
        //     $destination = $this->destinationDir;
        //     $namaFile = md5($fileFoto->getClientOriginalName() . time()).'.'.$fileFoto->extension();
        //     // $namaFile = str_random(10).'.'.$fileFoto->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($destination)){
        //         File::makeDirectory($destination, 0777);
        //     }
        //     $fileFoto->move($destination, $namaFile);

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                FotoDetail::create([
                    'foto_id' => $id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }

        // $fileFoto = $request->foto;
        // if($request->hasFile('foto')) {
        //     $store = $this->storeDir;
        //     $destination = $this->destinationDir;
        //     $namaFile = md5($fileFoto->getClientOriginalName() . time()).'.'.$fileFoto->extension();
        //     // $namaFile = str_random(10).'.'.$fileFoto->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($destination)){
        //         File::makeDirectory($destination, 0777);
        //     }
        //     $fileFoto->move($destination, $namaFile);

        //     $item = $this->table->findOrFail($id);
        //     if(!empty($item->file)){
        //         if(File::isDirectory($destination)){
        //             Storage::delete($this->storageDir.'/'.$item->file);
        //         }
        //     }

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);

        $item = FotoDetail::where('foto_id', $id);

        foreach ($item->get() as $key => $value) {
            if(!empty($value->file)){
                Storage::delete($this->storageDir.'/'.$value->file);
            }
        }
        $item->delete();
        $tb->delete();

        // $destination = $this->destinationDir;
        // if(!empty($tb->file)){
        //     if(File::isDirectory($destination)){
        //         Storage::delete($this->storageDir.'/'.$tb->file);
        //     }
        // }
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $foto = $this->table->findOrFail($id);

        $file = FotoDetail::where('foto_id', $id)
                ->orderBy('id', 'desc')
                ->pluck('file', 'id')->toArray();

        $data['foto'] = $foto;
        $data['form'] = $formBuilder->create('App\Forms\FotoForm', [
            'method' => 'POST',
            'model' => $foto,
            // 'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('title', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('content', 'textarea', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('foto', 'static', [
            'template' => 'admin.foto.detail_file',
            'value' => function ($val) use ($file) {
                return $file;
            }
        ])
        // ->remove('foto')
        // ->addAfter('content', 'file', 'static')->modify('file', 'static', [
        //     'template' => 'admin.foto.attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ])
        ->modify('status', 'choice', [
            'attr' => [
                'disabled' => '',
                'class' => 'disable'
            ]
        ]);
        // ->modify('file_attachment', 'static', [
        //     'template' => 'admin.topup.detail_attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ]);
        // $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.detail',$data);
    }

    public function viewFile($id)
    {
        $item = FotoDetail::findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }

    public function deleteFile($id)
    {
        $item = FotoDetail::findOrFail($id);
        if(!empty($item->file)){
            Storage::delete($this->storageDir.'/'.$item->file);
        }
        $item->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }
}
