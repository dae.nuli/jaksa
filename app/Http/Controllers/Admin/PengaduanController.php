<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FilePengaduan;
use App\Models\Pengaduan;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;
use DataTables;
use Form;
use File;

class PengaduanController extends Controller
{
    private $folder = 'admin.pengaduan';
    private $uri = 'admin.pengaduan';
    private $title = 'Pengaduan';
    private $desc = 'Description';


    public function __construct(Pengaduan $table, FilePengaduan $pengaduan) 
    {
        $this->middleware('permission:pengaduan_index', ['only' => ['index','data']]);
        $this->middleware('permission:pengaduan_detail', ['only' => ['detail']]);
        $this->middleware('permission:pengaduan_create', ['only' => ['create','store']]);
        $this->middleware('permission:pengaduan_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:pengaduan_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->filePengaduan = $pengaduan;
        $this->destinationDir = base_path('public/store/pengaduan');
        $this->storeDir = base_path('public/store');
        $this->storageDir = 'store/pengaduan';
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = ($request->status != null) ? route($this->uri.'.data', $request->status) : route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        $data['status'] = $request->status;
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request, $status = null)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'user_id', 'nama_pengadu', 'nama_kegiatan', 'created_at', 'status'])
                    ->whereHas('user', function ($query) {
                        $query->where('status', 1);
                    });
            if ($status != null) {
                $data = $data->where('status', $status);
            }
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('status', function ($index) {
                    if($index->status == 1) {
                        return '<span class="label label-success">DITERIMA</span>';
                    } elseif ($index->status == 2) {
                        return '<span class="label label-danger">DITOLAK</span>';
                    } elseif ($index->status == 3) {
                        return '<span class="label label-primary">SEDANG PROSES</span>';
                    } elseif ($index->status == 4) {
                        return '<span class="label label-info">SELESAI</span>';
                    } else {
                        return '<span class="label label-warning">BARU</span>';
                    }
                })
                ->editColumn('user_id', function ($index) {
                    return isset($index->user->name) ? $index->user->name : '-';
                })
                // ->editColumn('monthly_price', function ($index) {
                //     return 'Rp '.number_format($index->monthly_price, 0, '', '.');
                // })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('pengaduan_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth('admin')->user()->can('pengaduan_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    $tag .= (auth('admin')->user()->can('pengaduan_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\PengaduanForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $file = $this->filePengaduan->where('pengaduan_id', $id)
                ->orderBy('id', 'desc')
                ->pluck('file', 'id')->toArray();
        $data['form'] = $formBuilder->create('App\Forms\PengaduanForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('filepengaduan', 'static', [
            'template' => 'admin.pengaduan.files_edit',
            'value' => function ($val) use ($file) {
                return $file;
            }
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $row = $this->table->create($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                $this->filePengaduan->create([
                    'pengaduan_id' => $row->id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }

        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                $this->filePengaduan->create([
                    'pengaduan_id' => $id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }
        return redirect()->back()->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);

        $item = $this->filePengaduan->where('pengaduan_id', $id);

        foreach ($item->get() as $key => $value) {
            if(!empty($value->file)){
                Storage::delete($this->storageDir.'/'.$value->file);
            }
        }
        $item->delete();
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $row = $this->table->findOrFail($id);

        $file = $this->filePengaduan->where('pengaduan_id', $id)
                ->orderBy('id', 'desc')
                ->pluck('file', 'id')->toArray();

        $data['row'] = $row;
        $data['form'] = $formBuilder->create('App\Forms\PengaduanForm', [
            'method' => 'POST',
            'model' => $row,
            'url' => route($this->uri.'.confirmation', $id)
            // 'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('user_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('nama_pengadu', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('nama_kegiatan', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('permasalahan', 'textarea', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('penanggungjawab', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('phone', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        // ->modify('email', 'text', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        // ->modify('keterangan', 'text', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        ->modify('filepengaduan', 'static', [
            'template' => 'admin.pengaduan.attachment',
            'value' => function ($val) use ($file) {
                return $file;
            }
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);
        // ->modify('file_attachment', 'static', [
        //     'template' => 'admin.topup.detail_attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ]);
        // $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.detail',$data);
    }

    public function postConfirmation(Request $request, $id)
    {
        $request->validate([
            'keterangan' => 'required'
        ]);

        $this->table->findOrFail($id)->update($request->all());

        return redirect()->back()->with('success',trans('message.update'));
    }

    public function viewFile($id)
    {
        $item = $this->filePengaduan->findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }

    public function deleteFile($id)
    {
        $item = $this->filePengaduan->findOrFail($id);
        if(!empty($item->file)){
            Storage::delete($this->storageDir.'/'.$item->file);
        }
        $item->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }
}