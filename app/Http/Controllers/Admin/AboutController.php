<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;

class AboutController extends Controller
{
    private $folder = 'admin.about';
    private $uri = 'admin.about';
    private $title = 'About';
    private $desc = 'Description';


    public function __construct(About $table) 
    {
        $this->middleware('permission:contact_index', ['only' => ['index','update']]);
        $this->table = $table;
    }

    public function index()
    {
        $data['title'] = $this->title;
    	$data['about'] = About::find(1);
        return view($this->folder.'.index',$data);
    }

    public function update(Request $request, $id)
    {
    	About::find($id)->update($request->all());
    	return redirect()->back()->with('about', 'Data sudah diperbarui');
    }
}
