<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Kris\LaravelFormBuilder\FormBuilder;
use DataTables;
use Form;

class UserController extends Controller
{
    private $folder = 'admin.users';
    private $uri = 'admin.users';
    private $title = 'Instansi';
    private $desc = 'Description';


    public function __construct(User $table) 
    {
        $this->middleware('permission:user_index', ['only' => ['index','data']]);
        $this->middleware('permission:user_create', ['only' => ['create','store']]);
        $this->middleware('permission:user_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        // $data['ajax'] = ($request->status != null) ? route($this->uri.'.data', $request->status) : route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        $data['ajax'] = $this->fullUrl;
        $data['status'] = $request->status;
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'email', 'created_at', 'status']);
            // if ($status != null) {
            //     $data = $data->where('status', $status);
            // }
            if ($request->status != null) {
                $data = $data->where('status', $request->status);
            }
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('status', function ($index) {
                    if($index->status == 1) {
                        return '<span class="label label-success">ACTIVE</span>';
                    } elseif ($index->status == 2) {
                        return '<span class="label label-danger">NOT ACTIVE</span>';
                    } else {
                        return '<span class="label label-info">NEW</span>';
                    }
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('user_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth('admin')->user()->can('user_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\UserForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\UserForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ])
        ->modify('password', 'password', [
            'value' => '',
            'attr' => ['data-validation' => '']
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email'
        ]);
        $request->merge([
            'password' => bcrypt($request->password)
        ]);
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|unique:users,email,'.$id
        ]);
        if(empty($request->password)){
            unset($request['password']);
        } else {
            $request->merge([
                'password' => bcrypt($request->password)
            ]);
        }
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
