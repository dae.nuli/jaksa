<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Admin;

class ProfileController extends Controller
{
    private $folder = 'admin.profile';
    private $uri = 'admin.profile';
    private $title = 'Profile';
    private $desc = 'Description';


    public function __construct(Admin $table) 
    {
        $this->table = $table;
    }

    public function index(FormBuilder $formBuilder)
    {
        $tbl = $this->table->find(auth('admin')->user()->id);
        $data['form'] = $formBuilder->create('App\Forms\ProfileForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update')
        ]);

        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        return view($this->folder.'.index', $data);
    }

    public function update(Request $request)
    {
        $id = auth('admin')->user()->id;
        $request->validate([
            'password' => 'nullable|min:6|confirmed'
        ]);
        if(empty($request->password)){
            unset($request['password']);
        } else {
            $request->merge([
                'password' => bcrypt($request->password)
            ]);
        }
    	$this->table->findOrFail($id)->update($request->all());
    	return redirect()->back()->with('profile', 'Data sudah diperbarui');
    }
}
