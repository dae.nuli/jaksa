<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pengaduan;
use App\Models\Kegiatan;
use App\User;
use Voerro\Laravel\VisitorTracker\Models\Visit;
use DB;
// use Voerro\Laravel\VisitorTracker\VisitStats;
use Voerro\Laravel\VisitorTracker\Facades\VisitStats;

class HomeController extends Controller
{


    public function __construct(Kegiatan $table, User $user) 
    {
        $this->table = $table;
        $this->user = $user;
    }
    public function index()
    {
    	$data['title'] = 'Dashboard';
		$data['page_description'] = 'Dashboard';
		$data['users'] = User::count();
		$data['newUsers'] = User::where('status', 1)->count();
		$data['pengaduan'] = Pengaduan::count();
		$data['kegiatan'] = Kegiatan::count();
		$data['eventNew'] = $this->table->where('status', 0)->count();
		$data['eventProces'] = $this->table->where('status', 3)->count();
		$data['eventAccept'] = $this->table->where('status', 1)->count();
		$data['eventDone'] = $this->table->where('status', 4)->count();
		$data['eventReject'] = $this->table->where('status', 2)->count();
		$data['usersNew'] = $this->user->where('status', 0)->count();
		$data['usersActive'] = $this->user->where('status', 1)->count();
		$data['usersInActive'] = $this->user->where('status', 2)->count();
		$data['visitor'] = VisitStats::query()
                ->visits()
                ->withUsers()
                ->except(['ajax', 'bots', 'login_attempts'])
                ->orderBy('visitors_count', 'DESC')
                ->groupBy('ip')->count();
    	return view('admin.home.index', $data);
    }
}
