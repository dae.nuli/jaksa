<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FileKegiatan;
use App\Models\Kegiatan;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;
use App\Models\QueueKegiatan;
use DataTables;
use Form;
use File;
use Str;
use PDF;
use DB;
use App\Exports\InvoicesExport;

class KegiatanController extends Controller
{
    private $folder = 'admin.kegiatan';
    private $uri = 'admin.kegiatan';
    private $title = 'Kegiatan';
    private $desc = 'Description';


    public function __construct(Kegiatan $table, Excel $excel) 
    {
        $this->middleware('permission:kegiatan_index', ['only' => ['index','data']]);
        $this->middleware('permission:kegiatan_detail', ['only' => ['detail']]);
        $this->middleware('permission:kegiatan_create', ['only' => ['create','store']]);
        $this->middleware('permission:kegiatan_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:kegiatan_delete', ['only' => ['destroy']]);
        $this->table = $table;
        $this->destinationDir = base_path('public/store/kegiatan');
        $this->storeDir = base_path('public/store');
        $this->storageDir = 'store/kegiatan';
        $this->excel = $excel;
        $this->fullUrl = isset(explode('?', url()->full())[1]) ? route($this->uri.'.data').'?'.explode('?', url()->full())[1] : route($this->uri.'.data');
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = $this->fullUrl;
        $data['create'] = route($this->uri.'.create');
        $data['status'] = $request->status;
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            // $data = $this->table->select(['id', 'user_id', 'instansi_pemohon', 'nama_kegiatan', 'pagu_anggaran', 'created_at', 'keterangan','status'])
            $data = $this->table->select(['id', 'user_id', 'nama_kegiatan', 'pagu_anggaran', 'created_at', 'keterangan','status'])
                ->whereHas('user', function ($query) {
                    $query->where('status', 1);
                });
            // if ($status != null) {
            //     $data = $data->where('status', $status);
            // }

            if ($request->status != null) {
                $data = $data->where('status', $request->status);
            }
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                ->editColumn('status', function ($index) {
                    if($index->status == 1) {
                        return '<span class="label label-success">DITERIMA</span>';
                    } elseif ($index->status == 2) {
                        return '<span class="label label-danger">DITOLAK</span>';
                    } elseif ($index->status == 3) {
                        return '<span class="label label-primary">SEDANG PROSES</span>';
                    } elseif ($index->status == 4) {
                        return '<span class="label label-info">SELESAI</span>';
                    } else {
                        return '<span class="label label-warning">BARU</span>';
                    }
                })
                ->editColumn('pagu_anggaran', function ($index) {
                    return 'Rp '.number_format($index->pagu_anggaran, 0, '', '.');
                })
                ->editColumn('user_id', function ($index) {
                    return Str::words($index->user->name, 3);
                })
                ->editColumn('keterangan', function ($index) {
                    return Str::words($index->keterangan, 3);
                })
                ->editColumn('nama_kegiatan', function ($index) {
                    return Str::words($index->nama_kegiatan, 3);
                })
                ->editColumn('user_id', function ($index) {
                    return isset($index->user->name) ? $index->user->name : '-';
                })
                ->editColumn('keterangan', function ($index) {
                    return empty($index->keterangan) ? '-' : $index->keterangan;
                })
                ->editColumn('created_at', function ($index) {
                    return date('d F Y', strtotime($index->created_at));
                })
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth('admin')->user()->can('kegiatan_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    $tag .= (auth('admin')->user()->can('kegiatan_detail')) ? " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>" : '';
                    $tag .= (auth('admin')->user()->can('kegiatan_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'status', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\KegiatanForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $file = FileKegiatan::where('kegiatan_id', $id)
                ->orderBy('id', 'desc')
                ->pluck('file', 'id')->toArray();

        $data['form'] = $formBuilder->create('App\Forms\KegiatanForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ])
        ->modify('jadwal_sosialisasi', 'text', [
            'value' => function ($val) {
                return !empty($val) ? $val : null;
            }
        ])
        ->modify('mulai_spmk', 'text', [
            'value' => function ($val) {
                return !empty($val) ? $val : null;
            }
            // 'value' => null
        ])
        ->modify('berakhir_spmk', 'text', [
            'value' => function ($val) {
                return !empty($val) ? $val : null;
            }
            // 'value' => null
        ])
        ->modify('jadwal_monitoring', 'text', [
            'value' => function ($val) {
                return !empty($val) ? $val : null;
            }
            // 'value' => null
        ])
        ->modify('filekegiatan', 'static', [
            'template' => 'admin.kegiatan.files_edit',
            'value' => function ($val) use ($file) {
                return $file;
            }
        ])
        ->modify('status', 'choice', [
            'selected' => null
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $row = $this->table->create($request->all());

        if (isset($request->jadwal_sosialisasi) && !empty($request->jadwal_sosialisasi)) {
            QueueKegiatan::create([
                'kegiatan_id' => $row->id,
                'status' => 0,
                'created_at' => date('Y-m-d').' 00:00:00'
            ]);
        }
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                FileKegiatan::create([
                    'kegiatan_id' => $row->id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }
        // dd($request->attach);
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        

        if (isset($request->jadwal_sosialisasi) && !empty($request->jadwal_sosialisasi)) {
            QueueKegiatan::updateOrCreate(
                ['kegiatan_id' => $id],
                // 'status' => 0,
                ['created_at' => date('Y-m-d').' 00:00:00']
            );
        }

        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                FileKegiatan::create([
                    'kegiatan_id' => $id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }

        // $fileKegiatan = $request->filekegiatan;
        // if($request->hasFile('filekegiatan')) {
        //     $store = base_path('public/store');
        //     $destination = base_path('public/store/kegiatan');
        //     $namaFile = str_random(20).'.'.$fileKegiatan->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($destination)){
        //         File::makeDirectory($destination, 0777);
        //     }
        //     $fileKegiatan->move($destination, $namaFile);

        //     $item = $this->table->findOrFail($id);
        //     if(!empty($item->file)){
        //         if(File::isDirectory($destination)){
        //             Storage::delete('store/kegiatan/'.$item->file);
        //         }
        //     }

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }
        // $this->table->findOrFail($id)->update($request->all());
        return redirect()->back()->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        QueueKegiatan::where('kegiatan_id', $id)->delete();

        $item = FileKegiatan::where('kegiatan_id', $id);

        foreach ($item->get() as $key => $value) {
            if(!empty($value->file)){
                Storage::delete($this->storageDir.'/'.$value->file);
            }
        }
        $item->delete();
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }

    public function show($id, FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $kegiatan = $this->table->findOrFail($id);

        $file = FileKegiatan::where('kegiatan_id', $id)
                ->orderBy('id', 'desc')
                ->pluck('file', 'id')->toArray();
        $data['kegiatan'] = $kegiatan;
        $data['form'] = $formBuilder->create('App\Forms\KegiatanForm', [
            'method' => 'POST',
            'model' => $kegiatan,
            'url' => route($this->uri.'.confirmation', $id)
        ])
        ->modify('user_id', 'select', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        // ->modify('instansi_pemohon', 'text', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        ->modify('nama_kegiatan', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('jenis_pekerjaan', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        // ->modify('penyerapan_anggaran', 'text', [
        //     'attr' => [
        //         'disabled' => '',
        //     ]
        // ])
        ->modify('pagu_anggaran', 'text', [
            'attr' => [
                'disabled' => '',
            ],
            'value' => function ($val) {
                return 'Rp '.number_format($val, 0, '', '.');
            }
        ])
        ->modify('jadwal_sosialisasi', 'text', [
            'value' => null
        ])
        ->modify('mulai_spmk', 'text', [
            'value' => null
        ])
        ->modify('berakhir_spmk', 'text', [
            'value' => null
        ])
        ->modify('jadwal_monitoring', 'text', [
            'value' => null
        ])
        ->modify('bentuk_pengawalan', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('nama_penanggungjawab', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('persentase_permasalahan', 'number', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('tahapan_proyek', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('phone', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('sumber_dana', 'text', [
            'attr' => [
                'disabled' => '',
            ]
        ])
        ->modify('keterangan', 'text', [
            'attr' => ['data-validation' => 'required']
        ])
        ->modify('nomor_surat_perintah', 'text', [
            'attr' => ['data-validation' => 'required']
        ])
        ->modify('tim_pelaksana', 'text', [
            'attr' => ['data-validation' => 'required']
        ])
        ->modify('tahapan_pengawalan', 'text', [
            'attr' => ['data-validation' => 'required']
        ])
        ->modify('masa_pelaksanaan', 'text', [
            'attr' => ['data-validation' => 'required']
        ])
        ->modify('filekegiatan', 'static', [
            'template' => 'admin.kegiatan.attachment',
            'value' => function ($val) use ($file) {
                return $file;
            }
        ])
        ->modify('status', 'choice', [
            'selected' => null,
        ]);
        // ->modify('file_attachment', 'static', [
        //     'template' => 'admin.topup.detail_attachment',
        //     'value' => function ($val) {
        //         return $val;
        //     }
        // ]);
        // $data['title'] = $this->title;
        $data['subTitle'] = 'Detail';

        $data['url'] = route($this->uri.'.index');

        return view($this->folder.'.detail',$data);
    }

    public function postConfirmation(Request $request, $id)
    {
        $request->validate([
            'keterangan' => 'required'
        ]);

        if (isset($request->jadwal_sosialisasi) && !empty($request->jadwal_sosialisasi)) {
            QueueKegiatan::updateOrCreate(
                ['kegiatan_id' => $id],
                // 'status' => 0,
                ['created_at' => date('Y-m-d').' 00:00:00']
            );
        }
        
        $this->table->findOrFail($id)->update($request->all());

        return redirect()->back()->with('success',trans('message.update'));
    }

    public function viewFile($id)
    {
        $item = FileKegiatan::findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }

    public function deleteFile($id)
    {
        $item = FileKegiatan::findOrFail($id);
        if(!empty($item->file)){
            Storage::delete($this->storageDir.'/'.$item->file);
        }
        $item->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }

    public function download(Request $request)
    {
        $data['data'] = $this->table->where('status', $request->status)
                    // ->whereBetween(DB::raw('DATE(created_at)'), [$request->start, $request->end])
                    ->whereMonth('created_at', $request->month)
                    ->orderBy('id', 'desc')
                    ->get();
        $data['month'] = $this->months($request->month);
        $data['year'] = $request->year;
        $data['status'] = $this->status($request->status);

        if ($request->document_type == 1) {
            $pdf = PDF::loadView('admin.kegiatan.download_pdf', $data);
            return $pdf->setPaper('a4', 'landscape')->download(time().'.pdf');
        } elseif ($request->document_type == 2) {
            return $this->excel->download(new InvoicesExport($data), time().'.xlsx');
        }
    }

    public function status($id)
    {
        switch ($id) {
            case 1:
                return 'DITERIMA';
                break;
            case 2:
                return 'DITOLAK';
                break;
            case 3:
                return 'SEDANG PROSES';
                break;
            case 4:
                return 'SELESAI';
                break;
            
            default:
                return 'BARU';
                break;
        }
    }

    public function months($id)
    {
        switch ($id) {
            case 1:
                return 'JANUARI';
                break;
            case 2:
                return 'FEBRUARI';
                break;
            case 3:
                return 'MARET';
                break;
            case 4:
                return 'APRIL';
                break;
            case 5:
                return 'MEI';
                break;
            case 6:
                return 'JUNI';
                break;
            case 7:
                return 'JULI';
                break;
            case 8:
                return 'AGUSTUS';
                break;
            case 9:
                return 'SEPTEMBER';
                break;
            case 10:
                return 'OKTOBER';
                break;
            case 11:
                return 'NOVEMBER';
                break;
            case 12:
                return 'DESEMBER';
                break;
        }
    }

}
