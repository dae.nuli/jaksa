<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\UserRegisteredSuccessfully;
use Illuminate\Http\Request;
use App\Models\FilePengaduan;
use App\Models\FileKegiatan;
use App\Models\Pengaduan;
use App\Models\Kegiatan;
use App\Models\Undang;
use App\Models\Video;
use App\Models\FotoDetail;
use App\Models\Foto;
use App\Models\Slider;
use App\Models\KerjaNyata;
use App\User;
use File;


class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    private $folder = 'front';
    public function __construct(Kegiatan $kegiatan, Pengaduan $pengaduan)
    {
        $this->kegiatan = $kegiatan;
        $this->pengaduan = $pengaduan;
        $this->storeDir = base_path('public/store');
        $this->kegiatanDir = base_path('public/store/kegiatan');
        $this->pengaduanDir = base_path('public/store/pengaduan');
        $this->uuDir = base_path('public/store/uu');
        $this->kerjaDir = base_path('public/store/kerja');
    }

    public function index()
    {
        $data['pageTitle'] = 'Home';
        $data['kegiatan'] = Kegiatan::where('status', '!=', 0)
                            ->orderBy('id', 'desc')
                            ->limit(10)->get();
        $data['foto'] = Foto::where('status', 1)->orderBy('id', 'desc')->limit(10)->get();
        $data['slider'] = Slider::where('status', 1)->orderBy('id', 'desc')->get();
        $data['kerja'] = KerjaNyata::where('status', 1)->orderBy('id', 'desc')->get();
        return view($this->folder.'.home.index', $data);
    }

    public function permohonanIndex()
    {
        $data['pageTitle'] = 'Permohonan';
        $data['pageDesc'] = title_case('form permohonan tp4d wilayah hukum kejati nunukan');
        return view($this->folder.'.permohonan.index', $data);
    }

    public function permohonanStore(Request $request)
    {
        $request->validate([
            'instansi_pemohon' => 'required|max:255',
            'nama_kegiatan' => 'required|max:255',
            'jenis_pekerjaan' => 'required|max:255',
            // 'pagu_anggaran' => 'required',
            // 'sumber_dana' => 'required',
            'tahapan_proyek' => 'required',
            'bentuk_pengawalan' => 'required',
            'nama_penanggungjawab' => 'required',
            'persentase_permasalahan' => 'required|numeric',
            'phone' => 'required|numeric',
            'password' => 'required|min:6',
            'email' => 'required|email|unique:users,email',
            'captcha' => 'required|captcha'
            // 'g-recaptcha-response' => 'required|captcha'
        ]);

        // if($request->hasFile('attach')) {

            $user = User::create([
                'name' => $request->instansi_pemohon,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'status' => 0,
                'activation_code' => str_random(30).time()
            ]);
            // return 'xx';
            $user->notify(new UserRegisteredSuccessfully($user));
            
            $request->merge(['user_id' => $user->id]);
            $row = $this->kegiatan->create($request->all());

            // $store = $this->storeDir;
            // $destination = $this->kegiatanDir;
            
            // if(!File::isDirectory($store)){
            //     File::makeDirectory($store, 0777);    
            // }
            
            // if(!File::isDirectory($destination)){
            //     File::makeDirectory($destination, 0777);
            // }

            // foreach ($request->attach as $key => $value) {
            //     $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
            //     FileKegiatan::create([
            //         'kegiatan_id' => $row->id,
            //         'file' => $namaFile
            //     ]);

            //     $value->move($destination, $namaFile);
            // }

        // } else {
            // return redirect()->back()->withInput($request->except('password'))->with('file_required', trans('message.file_required'));
        // }

        return redirect()->back()->with('status', trans('message.permohonan'));
        // Auth::loginUsingId($user->id);
        // return redirect(route('permohonan.index'))->with('status',trans('message.create'));
    }

    public function pengaduanIndex()
    {
        $data['pageTitle'] = 'Pengaduan';
        $data['pageDesc'] = title_case('form pengaduan tp4d wilayah hukum kejati nunukan');
        return view($this->folder.'.pengaduan.index', $data);
    }

    public function pengaduanStore(Request $request)
    {
        $request->validate([
            'nama_pengadu' => 'required|max:255',
            'nama_kegiatan' => 'required|max:255',
            'permasalahan' => 'required',
            'penanggungjawab' => 'required',
            'phone' => 'required|numeric',
            'password' => 'required|min:6',
            'email' => 'required|email|unique:users,email',
            'captcha' => 'required|captcha'
            // 'g-recaptcha-response' => 'required|captcha'
        ]);

        if($request->hasFile('attach')) {

            $user = User::create([
                'name' => $request->nama_pengadu,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'status' => 0,
                'activation_code' => str_random(30).time()
            ]);

            $user->notify(new UserRegisteredSuccessfully($user));
            $request->merge(['user_id' => $user->id]);
            $row = $this->pengaduan->create($request->all());

            $store = $this->storeDir;
            $destination = $this->pengaduanDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                FilePengaduan::create([
                    'pengaduan_id' => $row->id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        } else {
            return redirect()->back()->withInput($request->except('password'))->with('file_required', trans('message.file_required'));
        }
        // $filePengaduan = $request->filepengaduan;
        // if($request->hasFile('filepengaduan')) {
        //     $store = $this->storeDir;
        //     $directoryPengaduan = $this->pengaduanDir;
        //     $namaFile = str_random(10).'.'.$filePengaduan->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($directoryPengaduan)){
        //         File::makeDirectory($directoryPengaduan, 0777);
        //     }
        //     $filePengaduan->move($directoryPengaduan, $namaFile);

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }

        // $request->merge(['user_id' => $user->id]);
        // $this->pengaduan->create($request->all());
        // Auth::loginUsingId($user->id);
        return redirect()->back()->with('status', trans('message.pengaduan'));
        // return redirect(route('pengaduan.index'))->with('status',trans('message.create'));
    }

    public function mekanismeIndex()
    {
        $data['pageTitle'] = 'Mekanisme';
        $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        return view($this->folder.'.mekanisme.index', $data);
    }

    public function kegiatanIndex()
    {
        $data['pageTitle'] = 'Kegiatan';
        $data['pageDesc'] = 'Kegiatan tp4d';
        $data['kegiatan'] = Kegiatan::where('status', '!=', 0)
                            ->orderBy('id', 'desc')
                            ->paginate(10);
        return view($this->folder.'.kegiatan.index', $data);
    }

    public function fotoIndex()
    {
        $data['pageTitle'] = 'Foto';
        // $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        $data['foto'] = Foto::where('status', 1)
                        ->orderBy('id', 'desc')
                        ->paginate(10);
        return view($this->folder.'.foto.index', $data);
    }

    public function fotoDetail($id)
    {
        $data['pageTitle'] = 'Foto';
        // $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        $data['detail'] = Foto::find($id);
        $data['detailfoto'] = FotoDetail::where('foto_id',$id)
                            ->orderBy('id', 'desc')
                            ->get();
        $data['related'] = Foto::where('id', '!=', $id)
                        ->where('status', 1)
                        ->orderBy('id', 'desc')
                        ->limit(10)->get();
        return view($this->folder.'.foto.detail', $data);
    }

    public function videoIndex()
    {
        $data['pageTitle'] = 'Video';
        $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        $data['video'] = Video::where('status', 1)
                        ->orderBy('id', 'desc')
                        ->paginate(4);
        return view($this->folder.'.video.index', $data);
    }

    public function videoDetail($id)
    {
        $data['pageTitle'] = 'Video';
        $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        $data['detail'] = Video::find($id);
        $data['video'] = Video::where('id', '!=', $id)
                        ->where('status', 1)
                        ->orderBy('id', 'desc')
                        ->limit(4)->get();
        return view($this->folder.'.video.detail', $data);
    }

    public function UUIndex()
    {
        $data['pageTitle'] = 'Undang Undang';
        $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        $data['undang'] = Undang::where('status', 1)
                        ->orderBy('id', 'desc')
                        ->paginate(4);
        return view($this->folder.'.uu.index', $data);
    }

    public function UUDetail($id)
    {
        $data['pageTitle'] = 'Undang Undang';
        $data['pageDesc'] = 'Showcase of Our Awesome Works in 5 Columns';
        $data['detail'] = Undang::find($id);
        $data['undang'] = Undang::where('id', '!=', $id)
                        ->where('status', 1)
                        ->orderBy('id', 'desc')
                        ->limit(4)->get();
        return view($this->folder.'.uu.detail', $data);
    }

    public function UUView($id)
    {
        $item = Undang::find($id);
        return response()->file($this->uuDir.'/'.$item->file);
    }

    // public function viewFile($id)
    // {
    //     $item = KerjaNyata::findOrFail($id);
    //     return response()->file($this->kerjaDir.'/'.$item->file);
    // }
}
