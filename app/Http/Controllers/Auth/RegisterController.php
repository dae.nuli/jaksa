<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Notifications\UserRegisteredSuccessfully;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/client/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $data['title'] = 'Register';
        return view('auth.register', $data);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => 0,
            'activation_code' => str_random(30).time()
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);
        $user->notify(new UserRegisteredSuccessfully($user));

        return $this->registered($request, $user)
                        ?: redirect('login')->with('status', 'Please check your email to activate the account');
                        // ?: redirect($this->redirectPath())->with('status', 'Please check your email to activate the account');
    }

    public function activateUser(string $activationCode)
    {
        try {
            $user = User::where('activation_code', $activationCode)
                    ->first();
            
            if (!$user) {
                return 'Activation code does not exist.';
            }

            $user->status = 1;
            $user->activation_code = null;
            $user->save();
            
            Auth::loginUsingId($user->id);

            return redirect('/client/home')->with('status',trans('message.activated'));

        } catch (Exception $e) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
    }
}
