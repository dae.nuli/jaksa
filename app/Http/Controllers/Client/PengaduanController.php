<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\FilePengaduan;
use App\Models\Pengaduan;
use Illuminate\Support\Facades\Storage;
use File;

class PengaduanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $folder = 'client.pengaduan';
    private $uri = 'pengaduan';
    private $title = 'PENGADUAN';
    private $desc = 'Description';

    public function __construct(Pengaduan $table, FilePengaduan $pengaduan)
    {
        $this->middleware('auth');
        $this->table = $table;
        $this->destinationDir = base_path('public/store/pengaduan');
        $this->storeDir = base_path('public/store');
        $this->storageDir = 'store/pengaduan';
        $this->filePengaduan = $pengaduan;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pengaduan'] = $this->table->where('user_id', Auth::id())
                            ->orderBy('id', 'desc')
                            ->paginate(10);
        $data['create'] = route($this->uri.'.create');
        $data['title'] = $this->title;
        return view($this->folder.'.index', $data);
    }

    public function create()
    {
        $data['action'] = route($this->uri.'.store');
        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_pengadu' => 'required|max:255',
            'nama_kegiatan' => 'required|max:255',
            'permasalahan' => 'required',
            'penanggungjawab' => 'required',
            'phone' => 'required|numeric'
        ]);

        $request->merge(['user_id' => Auth::id()]);
        $row = $this->table->create($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                $this->filePengaduan->create([
                    'pengaduan_id' => $row->id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        } else {
            return redirect()->back()->withInput()->with('file_required', trans('message.file_required'));
        }

        return redirect(route($this->uri.'.index'))->with('status',trans('message.create'));
    }

    public function edit($id)
    {
        $data['row'] = $this->table->find($id);
        $data['action'] = route($this->uri.'.update', $id);
        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        $data['dataFile'] = FilePengaduan::where('pengaduan_id', $id)
                            ->orderBy('id', 'desc')
                            ->pluck('file', 'id')->toArray();
        return view($this->folder.'.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_pengadu' => 'required|max:255',
            'nama_kegiatan' => 'required|max:255',
            'permasalahan' => 'required',
            'penanggungjawab' => 'required',
            'phone' => 'required|numeric'
        ]);

        $this->table->findOrFail($id)->update($request->all());
        // $this->table->findOrFail($id)->update($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                $this->filePengaduan->create([
                    'pengaduan_id' => $id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }
        return redirect()->back()->with('status', trans('message.update'));


        // $filePengaduan = $request->filepengaduan;
        // if($request->hasFile('filepengaduan')) {
        //     $store = $this->store;
        //     $destination = $this->destinationDir;
        //     $namaFile = str_random(10).'.'.$filePengaduan->extension();
        //     if(!File::isDirectory($store)){
        //         File::makeDirectory($store, 0777);    
        //     }
        //     if(!File::isDirectory($destination)){
        //         File::makeDirectory($destination, 0777);
        //     }
        //     $filePengaduan->move($destination, $namaFile);

        //     $item = $this->table->findOrFail($id);
        //     if(!empty($item->file)){
        //         if(File::isDirectory($destination)){
        //             Storage::delete('store/pengaduan/'.$item->file);
        //         }
        //     }

        //     $request->merge(['file' => $namaFile]);
        // }else{
        //     unset($request['file']);
        // }
        // $this->table->findOrFail($id)->update($request->all());
        // return redirect(route($this->uri.'.index'))->with('status', trans('message.update'));
    }

    public function show($id)
    {
        $data['row'] = $this->table->findOrFail($id);
        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        $data['dataFile'] = FilePengaduan::where('pengaduan_id', $id)
                            ->orderBy('id', 'desc')
                            ->pluck('file', 'id')->toArray();
        return view($this->folder.'.detail', $data);
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);

        $item = $this->filePengaduan->where('pengaduan_id', $id);

        foreach ($item->get() as $key => $value) {
            if(!empty($value->file)){
                Storage::delete($this->storageDir.'/'.$value->file);
            }
        }
        $item->delete();
        $tb->delete();
        return redirect()->back()->with('status', trans('message.delete'));
    }

    public function viewFile($id)
    {
        $item = $this->filePengaduan->findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }

    public function deleteFile($id)
    {
        $item = $this->filePengaduan->findOrFail($id);
        if(!empty($item->file)){
            Storage::delete($this->storageDir.'/'.$item->file);
        }
        $item->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }

}
