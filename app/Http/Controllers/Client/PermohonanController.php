<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\FileKegiatan;
use App\Models\Kegiatan;
use Illuminate\Support\Facades\Storage;
use File;

class PermohonanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $folder = 'client.permohonan';
    private $uri = 'permohonan';
    private $title = 'PERMOHONAN';
    private $desc = 'Description';

    public function __construct(Kegiatan $table, FileKegiatan $kegiatan)
    {
        $this->middleware('auth');
        $this->table = $table;
        $this->destinationDir = base_path('public/store/kegiatan');
        $this->storeDir = base_path('public/store');
        $this->storageDir = 'store/kegiatan';
        $this->filePermohonan = $kegiatan;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['kegiatan'] = $this->table->where('user_id', Auth::id())
                            ->orderBy('id', 'desc')
                            ->paginate(10);
        $data['create'] = route($this->uri.'.create');
        $data['title'] = $this->title;
        return view($this->folder.'.index', $data);
    }

    public function create()
    {
        $data['action'] = route($this->uri.'.store');
        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            // 'instansi_pemohon' => 'required|max:255',
            'nama_kegiatan' => 'required|max:255',
            'jenis_pekerjaan' => 'required|max:255',
            // 'pagu_anggaran' => 'required',
            // 'sumber_dana' => 'required',
            'tahapan_proyek' => 'required',
            'bentuk_pengawalan' => 'required',
            'nama_penanggungjawab' => 'required',
            'persentase_permasalahan' => 'required|numeric',
            'phone' => 'required|numeric'
        ]);

        $request->merge(['user_id' => Auth::id()]);
        $row = $this->table->create($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                $this->filePermohonan->create([
                    'kegiatan_id' => $row->id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        } else {
            return redirect()->back()->withInput()->with('file_required', trans('message.file_required'));
        }

        return redirect(route($this->uri.'.index'))->with('status',trans('message.create'));
    }

    public function edit($id)
    {
        $data['row'] = Kegiatan::find($id);
        $data['dataFile'] = FileKegiatan::where('kegiatan_id', $id)
                            ->orderBy('id', 'desc')
                            ->pluck('file', 'id')->toArray();
        $data['action'] = route($this->uri.'.update', $id);
        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        return view($this->folder.'.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            // 'instansi_pemohon' => 'required|max:255',
            'nama_kegiatan' => 'required|max:255',
            'jenis_pekerjaan' => 'required|max:255',
            // 'pagu_anggaran' => 'required',
            // 'sumber_dana' => 'required',
            'tahapan_proyek' => 'required',
            'bentuk_pengawalan' => 'required',
            'nama_penanggungjawab' => 'required',
            'persentase_permasalahan' => 'required|numeric',
            'phone' => 'required|numeric'
        ]);

        $this->table->findOrFail($id)->update($request->all());
        
        if($request->hasFile('attach')) {
            $store = $this->storeDir;
            $destination = $this->destinationDir;
            
            if(!File::isDirectory($store)){
                File::makeDirectory($store, 0777);    
            }
            
            if(!File::isDirectory($destination)){
                File::makeDirectory($destination, 0777);
            }

            foreach ($request->attach as $key => $value) {
                $namaFile = md5($value->getClientOriginalName() . time()).'.'.$value->extension();
                
                $this->filePermohonan->create([
                    'kegiatan_id' => $id,
                    'file' => $namaFile
                ]);

                $value->move($destination, $namaFile);
            }
        }
        return redirect()->back()->with('status', trans('message.update'));
    }

    public function show($id)
    {
        $data['row'] = Kegiatan::findOrFail($id);
        $data['url'] = route($this->uri.'.index');
        $data['title'] = $this->title;
        $data['dataFile'] = $this->filePermohonan->where('kegiatan_id', $id)
                            ->orderBy('id', 'desc')
                            ->pluck('file', 'id')->toArray();
        return view($this->folder.'.detail', $data);
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);

        $item = $this->filePermohonan->where('kegiatan_id', $id);

        foreach ($item->get() as $key => $value) {
            if(!empty($value->file)){
                Storage::delete($this->storageDir.'/'.$value->file);
            }
        }
        $item->delete();
        $tb->delete();
        return redirect()->back()->with('status', trans('message.delete'));
    }

    public function viewFile($id)
    {
        $item = $this->filePermohonan->findOrFail($id);
        return response()->file($this->destinationDir.'/'.$item->file);
    }

    public function deleteFile($id)
    {
        $item = $this->filePermohonan->findOrFail($id);
        if(!empty($item->file)){
            Storage::delete($this->storageDir.'/'.$item->file);
        }
        $item->delete();
        return response()->json(['msg' => true, 'success' => trans('message.delete')]);
    }

}
