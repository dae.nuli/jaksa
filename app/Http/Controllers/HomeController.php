<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QueueKegiatan;
use Carbon\Carbon;
use App\Models\ReminderStatus;
use App\Models\Kegiatan;
use App\Models\Foto;
use App\Notifications\SendReminder;
use Illuminate\Support\Facades\Auth;
use App\Admin;
use App\User;
use App\Models\Slider;
use App\Models\KerjaNyata;
use Notification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->kerjaDir = base_path('public/store/kerja');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'Home';
        $data['kegiatan'] = Kegiatan::where('status', '!=', 0)
                            ->orderBy('id', 'desc')
                            ->limit(10)->get();
        $data['foto'] = Foto::where('status', 1)->orderBy('id', 'desc')->limit(10)->get();
        $data['slider'] = Slider::where('status', 1)->orderBy('id', 'desc')->get();
        $data['kerja'] = KerjaNyata::where('status', 1)->orderBy('id', 'desc')->get();
        return view('home', $data);
    }

    public function KerjaView($id)
    {
        $item = KerjaNyata::findOrFail($id);
        return response()->file($this->kerjaDir.'/'.$item->file);
    }

    public function profile()
    {
        $data['title'] = 'Profile';
        $data['user'] = User::find(Auth::id());
        return view('profile', $data);
    }

    public function postProfile(Request $request)
    {
        $request->validate([
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        if(empty($request->password)){
            unset($request['password']);
        } else {
            // $request->merge([
            //     'password' => bcrypt($request->password)
            // ]);
            User::findOrFail(Auth::id())->update([
                'password' => bcrypt($request->password)
            ]);
            return redirect()->back()->with('success', trans('message.update'));
        }
        return redirect()->back();
    }

    public function testDel()
    {
        return QueueKegiatan::where('status', 1)->delete();
    }

    public function test()
    {
        $admin = Admin::find(1);
        $nowDate = Carbon::today();
        $qk = QueueKegiatan::orderBy('kegiatan_id')->where('status', 0)->get();
        if (count($qk)) {
            foreach ($qk as $key => $value) {
                if (!empty($value->created_at)) {
                    $kegiatan = Kegiatan::find($value->kegiatan_id);
                    // return $value->kegiatan->created_at;
                    $fromDate = Carbon::parse($value->created_at);
                    $endDate = Carbon::parse($value->kegiatan->jadwal_sosialisasi);
                    // dd($fromDate);
                    // $endDate = Carbon::parse($value->kegiatan->jadwal_sosialisasi)->format('Y-m-d');
                    $diff = $fromDate->diffInDays($endDate);
                    $div = floor($diff / 2);
                    $middleDate = $endDate->copy()->subDays($div);

                    // $dif[] = $fromDate.' - '.$endDate. ' : '.$diff. ' days';

                    if ($diff >= 4 && $diff <= 6) {
                        $aa[str_slug($value->kegiatan->nama_kegiatan, '_').'_'.str_slug($fromDate->format('Y-m-d'), '-').'_'.str_slug($endDate->format('Y-m-d'), '-')][] = $middleDate->format('d-m-Y');
                        if ($nowDate->eq($middleDate)) {

                            $days = $middleDate->copy()->diffInDays($endDate);

                            if (!$value->searchDate($value->id, $middleDate)) {
                                // ReminderStatus::create([
                                //     'queue_kegiatan_id' => $value->id,
                                //     'send_date' => $middleDate
                                // ]);
                                // Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                        }
                        
                    } elseif ($diff >= 7 && $diff <= 30) {

                        $twoDateBefore = $endDate->copy()->subDays(2);
                        $diffTwoDate = $fromDate->diffInDays($twoDateBefore);
                        $divTwo = floor($diffTwoDate / 2);
                        $middleTwoDate = $twoDateBefore->copy()->subDays($divTwo);

                        $aa[str_slug($value->kegiatan->nama_kegiatan, '_').'_'.str_slug($fromDate->format('Y-m-d'), '-').'_'.str_slug($endDate->format('Y-m-d'), '-')][] = $middleTwoDate->format('d-m-Y');
                        $aa[str_slug($value->kegiatan->nama_kegiatan, '_').'_'.str_slug($fromDate->format('Y-m-d'), '-').'_'.str_slug($endDate->format('Y-m-d'), '-')][] = $twoDateBefore->format('d-m-Y');

                        if ($nowDate->eq($middleTwoDate)) {

                            $days = $middleTwoDate->copy()->diffInDays($endDate);

                            if (!$value->searchDate($value->id, $middleTwoDate)) {
                                // ReminderStatus::create([
                                //     'queue_kegiatan_id' => $value->id,
                                //     'send_date' => $middleTwoDate
                                // ]);
                                // Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                        }

                        if ($nowDate->eq($twoDateBefore)) {

                            $days = $twoDateBefore->copy()->diffInDays($endDate);

                            if (!$value->searchDate($value->id, $twoDateBefore)) {
                                // ReminderStatus::create([
                                //     'queue_kegiatan_id' => $value->id,
                                //     'send_date' => $twoDateBefore
                                // ]);
                                // Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                        }

                    } elseif ($diff >= 31) {
                        $tenDateBefore = $endDate->copy()->subDays(4);
                        $diffTenDate = $fromDate->diffInDays($endDate);
                        $divTen = (int) floor($diffTenDate / 10) - 1;

                        for($i = 1; $i <= $divTen; $i++) {
                            $bef = $fromDate->copy()->addDays($i*10);

                            $aa[str_slug($value->kegiatan->nama_kegiatan, '_').'_'.str_slug($fromDate->format('Y-m-d'), '-').'_'.str_slug($endDate->format('Y-m-d'), '-')][$i] = $bef->format('d-m-Y');
                            $aa[str_slug($value->kegiatan->nama_kegiatan, '_').'_'.str_slug($fromDate->format('Y-m-d'), '-').'_'.str_slug($endDate->format('Y-m-d'), '-')][$i+1] = $tenDateBefore->format('d-m-Y');
                        }


                        // $middleTenDate = $tenDateBefore->copy()->subDays($divTen);

                        for($i = 1; $i <= $divTen; $i++) {
                            
                            $middleTenDate = $fromDate->copy()->addDays($i*10);
                            $days = $middleTenDate->copy()->diffInDays($endDate);

                            if ($nowDate->eq($middleTenDate)) {
                                if (!$value->searchDate($value->id, $middleTenDate)) {
                                    // ReminderStatus::create([
                                    //     'queue_kegiatan_id' => $value->id,
                                    //     'send_date' => $middleTenDate
                                    // ]);
                                    // Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                                }
                            }
                        }

                        if ($nowDate->eq($tenDateBefore)) {
                            if (!$value->searchDate($value->id, $tenDateBefore)) {
                                $days = $tenDateBefore->copy()->diffInDays($endDate);
                                // ReminderStatus::create([
                                //     'queue_kegiatan_id' => $value->id,
                                //     'send_date' => $tenDateBefore
                                // ]);
                                // Notification::route('mail',env('MAIL_FROM_ADDRESS'))->notify(new SendReminder($kegiatan, $days));
                            }
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                        }
                    }
                }
            }
            // dd($num);
            dd($aa);
            // dd($data);
        }
    }

    public function autoSendEmail()
    {
        $nowDate = Carbon::today();
        $qk = QueueKegiatan::orderBy('kegiatan_id')->where('status', 0)->get();
        if (count($qk)) {
            foreach ($qk as $key => $value) {
                if (!empty($value->created_at)) {
                    $fromDate = Carbon::parse($value->created_at);
                    $endDate = Carbon::parse($value->kegiatan->jadwal_sosialisasi);
                    $diff = $fromDate->diffInDays($endDate);
                    $div = floor($diff / 2);
                    $middle = $endDate->copy()->subDays($div);


                    if ($diff >= 4 && $diff <= 6) {
                        if ($nowDate->eq($middle)) {
                            return 'send';
                        }
                    } elseif ($diff >= 7) {

                        $twoDateBefore = $endDate->copy()->subDays(2);
                        $diffTwoDate = $fromDate->diffInDays($twoDateBefore);
                        $divTwo = floor($diffTwoDate / 2);
                        $middleTwoDate = $twoDateBefore->copy()->subDays($divTwo);

                        if ($nowDate->eq($middleTwoDate)) {
                            return 'send middle';
                        }

                        if ($nowDate->eq($twoDateBefore)) {
                            QueueKegiatan::findOrFail($value->id)->update(['status' => 1]);
                            return 'send 2 days before';
                        }

                    }
                }
            }
        }
    }
}
