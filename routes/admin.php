<?php

VisitStats::routes();
Route::get('/home', 'Admin\HomeController@index')->name('home');

Route::post('kegiatan/filter', 'Admin\KegiatanController@filter');
Route::post('kegiatan/download', 'Admin\KegiatanController@download');
Route::get('kegiatan/viewFile/{id}', 'Admin\KegiatanController@viewFile');
Route::get('kegiatan/deleteFile/{id}', 'Admin\KegiatanController@deleteFile');
Route::post('kegiatan/confirmation/{id}', 'Admin\KegiatanController@postConfirmation')->name('kegiatan.confirmation');
Route::get('kegiatan/data', 'Admin\KegiatanController@data')->name('kegiatan.data');
Route::resource('kegiatan', 'Admin\KegiatanController');

Route::get('users/data', 'Admin\UserController@data')->name('users.data');
Route::resource('users', 'Admin\UserController');

Route::get('admin/data', 'Admin\AdminController@data')->name('admin.data');
Route::resource('admin', 'Admin\AdminController');

Route::get('permission/data', 'Admin\PermissionController@data')->name('permission.data');
Route::resource('permission', 'Admin\PermissionController');

Route::get('role/data', 'Admin\RoleController@data')->name('role.data');
Route::resource('role', 'Admin\RoleController');

Route::post('pengaduan/filter', 'Admin\PengaduanController@filter');
Route::get('pengaduan/viewFile/{id}', 'Admin\PengaduanController@viewFile');
Route::get('pengaduan/deleteFile/{id}', 'Admin\PengaduanController@deleteFile');
Route::post('pengaduan/confirmation/{id}', 'Admin\PengaduanController@postConfirmation')->name('pengaduan.confirmation');
Route::get('pengaduan/data/{status?}', 'Admin\PengaduanController@data')->name('pengaduan.data');
Route::resource('pengaduan', 'Admin\PengaduanController');

Route::get('foto/viewFile/{id}', 'Admin\FotoController@viewFile');
Route::get('foto/deleteFile/{id}', 'Admin\FotoController@deleteFile');
Route::get('foto/data', 'Admin\FotoController@data')->name('foto.data');
Route::resource('foto', 'Admin\FotoController');

Route::get('slider/viewFile/{id}', 'Admin\SliderController@viewFile');
Route::get('slider/deleteFile/{id}', 'Admin\SliderController@deleteFile');
Route::get('slider/data', 'Admin\SliderController@data')->name('slider.data');
Route::resource('slider', 'Admin\SliderController');

Route::get('kerjaNyata/viewFile/{id}', 'Admin\KerjaNyataController@viewFile');
Route::get('kerjaNyata/deleteFile/{id}', 'Admin\KerjaNyataController@deleteFile');
Route::get('kerjaNyata/data', 'Admin\KerjaNyataController@data')->name('kerjaNyata.data');
Route::resource('kerjaNyata', 'Admin\KerjaNyataController');

Route::get('video/data', 'Admin\VideoController@data')->name('video.data');
Route::resource('video', 'Admin\VideoController');

Route::get('uu/view/{id}', 'Admin\UndangController@viewFile');
Route::get('uu/data', 'Admin\UndangController@data')->name('uu.data');
Route::resource('uu', 'Admin\UndangController');

Route::get('contact', 'Admin\ContactController@index')->name('contact.index');
Route::put('contact/{id}', 'Admin\ContactController@update')->name('contact.update');

Route::get('profile', 'Admin\ProfileController@index')->name('profile.index');
Route::put('profile', 'Admin\ProfileController@update')->name('profile.update');

// Route::get('visitor', 'Admin\VisitorController@index')->name('visitor.index');

// Route::get('visitor', 'Admin\VisitorController@summary');
