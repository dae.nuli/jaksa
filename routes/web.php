<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/info', function () {
  dd(phpinfo());
});

Route::get('/test', 'HomeController@test');

Route::middleware('tracker')->group(function () {

  Route::get('/', 'Frontend\HomeController@index');

  Route::get('/permohonan', 'Frontend\HomeController@permohonanIndex');
  Route::post('/permohonan', 'Frontend\HomeController@permohonanStore');

  Route::get('/pengaduan', 'Frontend\HomeController@pengaduanIndex');
  Route::post('/pengaduan', 'Frontend\HomeController@pengaduanStore');

  Route::get('/mekanisme', 'Frontend\HomeController@mekanismeIndex');
  Route::get('/kegiatan', 'Frontend\HomeController@kegiatanIndex');
  Route::get('/foto', 'Frontend\HomeController@fotoIndex');
  Route::get('/foto/{id}', 'Frontend\HomeController@fotoDetail');
  Route::get('/video', 'Frontend\HomeController@videoIndex');
  Route::get('/video/{id}', 'Frontend\HomeController@videoDetail');

  Route::get('/undang', 'Frontend\HomeController@UUIndex');
  Route::get('/undang/{id}', 'Frontend\HomeController@UUDetail');
  Route::get('/undang/view/{id}', 'Frontend\HomeController@UUView');

  // Route::get('/', function () {
  //     return view('front.layouts.app');
  //     // return view('welcome');
  // });

  Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');
  Auth::routes();


  Route::group(['prefix' => 'client'], function () {
    Route::get('/home', 'HomeController@index')->name('client.home');
    Route::get('/kerja/{id}', 'HomeController@KerjaView');
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::post('/profile', 'HomeController@postProfile');

    Route::get('permohonan/viewFile/{id}', 'Client\PermohonanController@viewFile');
    Route::get('permohonan/deleteFile/{id}', 'Client\PermohonanController@deleteFile');
    Route::resource('/permohonan', 'Client\PermohonanController');

    Route::get('pengaduan/viewFile/{id}', 'Client\PengaduanController@viewFile');
    Route::get('pengaduan/deleteFile/{id}', 'Client\PengaduanController@deleteFile');
    Route::resource('/pengaduan', 'Client\PengaduanController');
  });

});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

