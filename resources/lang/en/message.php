<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'create' => 'Data berhasil ditambahkan.',
    'update' => 'Data berhasil diperbarui.',
    'delete' => 'Data berhasil dihapus.',
    'permohonan' => 'Permohonan berhasil disubmit, silahkan cek email anda untuk mengaktifkan akun.',
    'pengaduan' => 'Pengaduan berhasil disubmit, silahkan cek email anda untuk mengaktifkan akun.',
    'file_required' => 'File tidak boleh kosong',
    'activated' => 'Akun anda telah aktif'
];
