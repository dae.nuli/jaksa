@extends('layouts.app')

@section('head-script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <style type="text/css">
        .uploaded_image {
            margin-bottom: 0 !important;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    {{$title}}
                </div>

                <div class="card-body">
                    <form class="needs-validation">

                        {{-- <div class="form-group row">
                            <label for="instansi_pemohon" class="col-sm-4 col-form-label text-md-right">{{ __('Instansi Pemohon') }}</label>

                            <div class="col-md-6">
                                <input id="instansi_pemohon" type="text" class="form-control" disabled="" value="{{ $row->instansi_pemohon }}">
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="nama_kegiatan" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Kegiatan') }}</label>

                            <div class="col-md-6">
                                <input id="nama_kegiatan" type="text" class="form-control" disabled="" value="{{ $row->nama_kegiatan }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jenis_pekerjaan" class="col-sm-4 col-form-label text-md-right">{{ __('Jenis Pekerjaan/Proyek') }}</label>

                            <div class="col-md-6">
                                <input id="jenis_pekerjaan" type="text" class="form-control" disabled="" value="{{ $row->jenis_pekerjaan }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="pagu_anggaran" class="col-sm-4 col-form-label text-md-right">{{ __('Pagu Anggaran') }}</label>

                            <div class="col-md-6">
                                <input id="pagu_anggaran" type="text" class="form-control" disabled="" value="{{ number_format($row->pagu_anggaran, 0, '', '.') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sumber_dana" class="col-sm-4 col-form-label text-md-right">{{ __('Sumber Dana') }}</label>

                            <div class="col-md-6">
                                <input id="sumber_dana" type="text" class="form-control" disabled="" value="{{ $row->sumber_dana }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tahapan_proyek" class="col-sm-4 col-form-label text-md-right">{{ __('Tahapan Proyek Pekerjaan') }}</label>

                            <div class="col-md-6">
                                <input id="tahapan_proyek" type="text" class="form-control" disabled="" value="{{ $row->tahapan_proyek }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bentuk_pengawalan" class="col-sm-4 col-form-label text-md-right">{{ __('Bentuk Pengawalan & Pengamanan') }}</label>

                            <div class="col-md-6">
                                <input id="bentuk_pengawalan" type="text" class="form-control" disabled="" value="{{ $row->bentuk_pengawalan }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_penanggungjawab" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Penanggung Jawab') }}</label>

                            <div class="col-md-6">
                                <input id="nama_penanggungjawab" type="text" class="form-control" disabled="" value="{{ $row->nama_penanggungjawab }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="persentase_permasalahan" class="col-sm-4 col-form-label text-md-right">{{ __('Persentase Permasalahan') }}</label>

                            <div class="col-md-6">
                                <input id="persentase_permasalahan" type="text" class="form-control" disabled="" value="{{ $row->persentase_permasalahan }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-4 col-form-label text-md-right">{{ __('Telp') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" disabled="" value="{{ $row->phone }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="file" class="col-sm-4 col-form-label text-md-right">{{ __('File Lampiran') }}</label>

                            <div class="col-md-6">
                                <p class="uploaded_image">
                                    @foreach($dataFile as $id => $row)
                                        <a href="{{url('client/permohonan/viewFile/'.$id)}}" target="_blank" class="btn-link" title="">{{$row}} (<i class="fa fa-fw fa-download"></i>)</a> <br>
                                    @endforeach
                                </p>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">

                                <a class="btn btn btn-primary" href="{{ $url }}">
                                    {{ __('Kembali') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
