@extends('layouts.app')

@section('head-script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <style type="text/css">
        .uploaded_image {
            margin-bottom: 0 !important;
        }
    </style>
@endsection

@section('end-script')
    <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.priceformat.min.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.multifile.js') }}"></script>
    <script type="text/javascript">
    $('.number').priceFormat({
      prefix: '',
      thousandsSeparator: '.',
      clearPrefix:true,
      centsLimit: 0,
      // centsSeparator: '.',
      // clearOnEmpty: true
    });
    $('#multifile').multifile();
    $(document).on('click','.delete-file',function(e){
      e.preventDefault();
      var url = $(this).data('url');
      var $this = $(this);

      if(confirm('Are you sure want to delete ?')) {
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          success : function(data){
            if(data.msg) {
              $this.next().next().remove();
              $this.next().remove();
              $this.remove();
            }
          }
        }).always(function(data) {
            // $('#dataTableBuilder').DataTable().draw(false);
        });
        
      } else {

      }
    });
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            } else {
                waiting();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{$title}}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ $action }}" class="needs-validation" enctype="multipart/form-data" novalidate>
                        @csrf
                        @method('PUT')

                        {{-- <div class="form-group row">
                            <label for="instansi_pemohon" class="col-sm-4 col-form-label text-md-right">{{ __('Instansi Pemohon') }}</label>

                            <div class="col-md-6">
                                <input id="instansi_pemohon" type="text" class="form-control{{ $errors->has('instansi_pemohon') ? ' is-invalid' : '' }}" name="instansi_pemohon" autocomplete="off" value="{{ $row->instansi_pemohon }}" required autofocus>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('instansi_pemohon'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('instansi_pemohon') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
 --}}
                        <div class="form-group row">
                            <label for="nama_kegiatan" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Kegiatan') }}</label>

                            <div class="col-md-6">
                                <input id="nama_kegiatan" type="text" class="form-control{{ $errors->has('nama_kegiatan') ? ' is-invalid' : '' }}" name="nama_kegiatan" autocomplete="off" value="{{ $row->nama_kegiatan }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('nama_kegiatan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nama_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jenis_pekerjaan" class="col-sm-4 col-form-label text-md-right">{{ __('Jenis Pekerjaan/Proyek') }}</label>

                            <div class="col-md-6">
                                <input id="jenis_pekerjaan" type="text" class="form-control{{ $errors->has('jenis_pekerjaan') ? ' is-invalid' : '' }}" name="jenis_pekerjaan" autocomplete="off" value="{{ $row->jenis_pekerjaan }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('jenis_pekerjaan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('jenis_pekerjaan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       {{--  <div class="form-group row">
                            <label for="pagu_anggaran" class="col-sm-4 col-form-label text-md-right">{{ __('Pagu Anggaran') }}</label>

                            <div class="col-md-6">
                                <input id="pagu_anggaran" type="text" class="number form-control{{ $errors->has('pagu_anggaran') ? ' is-invalid' : '' }}" name="pagu_anggaran" autocomplete="off" value="{{ $row->pagu_anggaran }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('pagu_anggaran'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('pagu_anggaran') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sumber_dana" class="col-sm-4 col-form-label text-md-right">{{ __('Sumber Dana') }}</label>

                            <div class="col-md-6">
                                <input id="sumber_dana" type="text" class="form-control{{ $errors->has('sumber_dana') ? ' is-invalid' : '' }}" name="sumber_dana" autocomplete="off" value="{{ $row->sumber_dana }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('sumber_dana'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sumber_dana') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="tahapan_proyek" class="col-sm-4 col-form-label text-md-right">{{ __('Tahapan Proyek Pekerjaan') }}</label>

                            <div class="col-md-6">
                                <input id="tahapan_proyek" type="text" class="form-control{{ $errors->has('tahapan_proyek') ? ' is-invalid' : '' }}" name="tahapan_proyek" autocomplete="off" value="{{ $row->tahapan_proyek }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('tahapan_proyek'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tahapan_proyek') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bentuk_pengawalan" class="col-sm-4 col-form-label text-md-right">{{ __('Bentuk Pengawalan & Pengamanan') }}</label>

                            <div class="col-md-6">
                                <input id="bentuk_pengawalan" type="text" class="form-control{{ $errors->has('bentuk_pengawalan') ? ' is-invalid' : '' }}" name="bentuk_pengawalan" autocomplete="off" value="{{ $row->bentuk_pengawalan }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('bentuk_pengawalan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('bentuk_pengawalan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_penanggungjawab" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Penanggung Jawab') }}</label>

                            <div class="col-md-6">
                                <input id="nama_penanggungjawab" type="text" class="form-control{{ $errors->has('nama_penanggungjawab') ? ' is-invalid' : '' }}" name="nama_penanggungjawab" autocomplete="off" value="{{ $row->nama_penanggungjawab }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('nama_penanggungjawab'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nama_penanggungjawab') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="persentase_permasalahan" class="col-sm-4 col-form-label text-md-right">{{ __('Persentase Permasalahan') }}</label>

                            <div class="col-md-6">
                                <input id="persentase_permasalahan" type="number" class="form-control{{ $errors->has('persentase_permasalahan') ? ' is-invalid' : '' }}" name="persentase_permasalahan" autocomplete="off" value="{{ $row->persentase_permasalahan }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('persentase_permasalahan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('persentase_permasalahan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-4 col-form-label text-md-right">{{ __('Telp') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" autocomplete="off" value="{{ $row->phone }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="file" class="col-sm-4 col-form-label text-md-right">{{ __('File Lampiran') }}</label>

                            <div class="col-md-6">
                                <input type="file" class="form-control multifile" id="multifile" name="attach[]">
                                <p class="uploaded_image">
                                    @foreach($dataFile as $id => $row)
                                        <a href="#" data-url="{{url('client/permohonan/deleteFile/'.$id)}}" class="multifile_remove_input delete-file"><i class="fa fa-fw fa-times"></i></a>
                                        <a href="{{url('client/permohonan/viewFile/'.$id)}}" target="_blank" class="btn-link" title="">{{$row}} (<i class="fa fa-fw fa-download"></i>)</a> <br>
                                    @endforeach
                                </p>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>

                                <a class="btn btn-link" href="{{ $url }}">
                                    {{ __('Batal') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
