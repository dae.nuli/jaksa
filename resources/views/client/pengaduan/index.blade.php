@extends('layouts.app')

@section('end-script')
<script type="text/javascript">
    $(document).ready(function(){
        $(".button-create").click(function(){
            window.location.href = '{{$create}}';
        });
    })
  $(document).on('click','.delete',function(e){
    e.preventDefault();
    if(confirm('Are you sure want to delete ?')) {
      $("#form")[0].submit();
    } 
  });
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{$title}}
                    <button type="button" class="btn btn-primary btn-sm float-right button-create">Create</button>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Nama Pengadu</th>
                          <th scope="col">Nama Kegiatan</th>
                          <th scope="col">Date</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($pengaduan as $key => $row)
                          <tr>
                            <th>{{($pengaduan->currentPage()-1) * $pengaduan->perPage() + $key + 1}}</th>
                            <td>{{$row->nama_pengadu}}</td>
                            <td>{{$row->nama_kegiatan}}</td>
                            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
                            <td>
                                <form action="{{route('pengaduan.destroy', $row->id)}}" method="POST" id="form">
                                    @method('DELETE')
                                    @csrf
                                    <a href="{{route('pengaduan.edit', $row->id)}}" class="btn btn-primary btn-sm">Edit</a>
                                    <a href="{{route('pengaduan.show', $row->id)}}" class="btn btn-success btn-sm">Detail</a>
                                    <button type="submit" class="delete btn btn-danger btn-sm">Delete</button>
                                </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{$pengaduan->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
