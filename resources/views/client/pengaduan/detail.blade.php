@extends('layouts.app')

@section('head-script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <style type="text/css">
        .uploaded_image {
            margin-bottom: 0 !important;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    {{$title}}
                </div>

                <div class="card-body">
                    <form>

                        <div class="form-group row">
                            <label for="nama_pengadu" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Pengadu') }}</label>

                            <div class="col-md-6">
                                <input id="nama_pengadu" type="text" class="form-control" disabled="" value="{{ $row->nama_pengadu }}" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_kegiatan" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Kegiatan') }}</label>

                            <div class="col-md-6">
                                <input id="nama_kegiatan" type="text" class="form-control" disabled="" value="{{ $row->nama_kegiatan }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="penanggungjawab" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Penanggung Jawab') }}</label>

                            <div class="col-md-6">
                                <input id="penanggungjawab" type="text" class="form-control" disabled="" value="{{ $row->penanggungjawab }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-4 col-form-label text-md-right">{{ __('Telp') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" disabled="" value="{{ $row->phone }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="permasalahan" class="col-sm-4 col-form-label text-md-right">{{ __('Permasalahan') }}</label>

                            <div class="col-md-6">
                                <textarea id="permasalahan" rows="15" class="form-control" disabled="">{{ $row->permasalahan }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="file" class="col-sm-4 col-form-label text-md-right">{{ __('Dokumentasi Kegiatan') }}</label>

                            <div class="col-md-6">
                                <p class="uploaded_image">
                                    @foreach($dataFile as $id => $row)
                                        <a href="{{url('client/pengaduan/viewFile/'.$id)}}" target="_blank" class="btn-link" title="">{{$row}} (<i class="fa fa-fw fa-download"></i>)</a> <br>
                                    @endforeach
                                </p>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <a class="btn btn-primary" href="{{ $url }}">
                                    {{ __('Kembali') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
