@extends('layouts.app')

@section('head-script')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <style type="text/css">
        .uploaded_image {
            margin-bottom: 0 !important;
        }
    </style>
@endsection

@section('end-script')
    <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.multifile.js') }}"></script>

    <script type="text/javascript">
    $('#multifile').multifile();
    $(document).on('click','.delete-file',function(e){
      e.preventDefault();
      var url = $(this).data('url');
      var $this = $(this);

      if(confirm('Are you sure want to delete ?')) {
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          success : function(data){
            if(data.msg) {
              $this.next().next().remove();
              $this.next().remove();
              $this.remove();
            }
          }
        }).always(function(data) {
            // $('#dataTableBuilder').DataTable().draw(false);
        });
        
      } else {

      }
    });
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                waiting();
            }
            console.log(form.checkValidity());
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{$title}}
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ $action }}" class="needs-validation" enctype="multipart/form-data" novalidate>
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="nama_pengadu" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Pengadu') }}</label>

                            <div class="col-md-6">
                                <input id="nama_pengadu" type="text" class="form-control{{ $errors->has('nama_pengadu') ? ' is-invalid' : '' }}" name="nama_pengadu" autocomplete="off" value="{{ $row->nama_pengadu }}" required autofocus>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('nama_pengadu'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nama_pengadu') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_kegiatan" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Kegiatan') }}</label>

                            <div class="col-md-6">
                                <input id="nama_kegiatan" type="text" class="form-control{{ $errors->has('nama_kegiatan') ? ' is-invalid' : '' }}" name="nama_kegiatan" autocomplete="off" value="{{ $row->nama_kegiatan }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('nama_kegiatan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nama_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="nilai_kegiatan" class="col-sm-4 col-form-label text-md-right">{{ __('Nilai Kegiatan') }}</label>

                            <div class="col-md-6">
                                <input id="nilai_kegiatan" type="text" class="number form-control{{ $errors->has('nilai_kegiatan') ? ' is-invalid' : '' }}" name="nilai_kegiatan" autocomplete="off" value="{{ $row->nilai_kegiatan }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('nilai_kegiatan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nilai_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="penanggungjawab" class="col-sm-4 col-form-label text-md-right">{{ __('Nama Penanggung Jawab') }}</label>

                            <div class="col-md-6">
                                <input id="penanggungjawab" type="text" class="form-control{{ $errors->has('penanggungjawab') ? ' is-invalid' : '' }}" name="penanggungjawab" autocomplete="off" value="{{ $row->penanggungjawab }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('penanggungjawab'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('penanggungjawab') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-4 col-form-label text-md-right">{{ __('Telp') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" autocomplete="off" value="{{ $row->phone }}" required>
                                <div class="invalid-feedback">
                                  Please fill out this field.
                                </div>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="permasalahan" class="col-sm-4 col-form-label text-md-right">{{ __('Permasalahan') }}</label>

                            <div class="col-md-6">
                                <textarea id="permasalahan" minlength="150" rows="10" class="form-control{{ $errors->has('permasalahan') ? ' is-invalid' : '' }}" name="permasalahan" autocomplete="off" required>{{ $row->permasalahan }}</textarea>
                                <div class="invalid-feedback">
                                    This field must be at least 150 characters.
                                </div>
                                @if ($errors->has('permasalahan'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('permasalahan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="file" class="col-sm-4 col-form-label text-md-right">{{ __('File Lampiran') }}</label>

                            <div class="col-md-6">
                                <input class="form-control multifile" name="attach[]" type="file" id="multifile">
                                <p class="uploaded_image">
                                    @foreach($dataFile as $id => $row)
                                        <a href="#" data-url="{{url('client/pengaduan/deleteFile/'.$id)}}" class="multifile_remove_input delete-file"><i class="fa fa-fw fa-times"></i></a>
                                        <a href="{{url('client/pengaduan/viewFile/'.$id)}}" target="_blank" class="btn-link" title="">{{$row}} (<i class="fa fa-fw fa-download"></i>)</a> <br>
                                    @endforeach
                                </p>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>

                                <a class="btn btn-link" href="{{ $url }}">
                                    {{ __('Batal') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
