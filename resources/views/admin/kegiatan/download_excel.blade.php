<html>
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		tr{
			text-align: center !important;
		}
	</style>
</head>
<body>
	<table>
	    <thead>
		    <tr>
		        <th colspan="11">TABEL LAPORAN KEGIATAN</th>
		    </tr>
		    <tr>
		        <th colspan="11">TIM PENGAWALAN DAN PENGAMANAN PEMERINTAH DAN PEMBANGUNAN (TP4)</th>
		    </tr>
		    <tr>
		        <th colspan="11">KEJAKSAAN NEGERI NUNUKAN</th>
		    </tr>
		    <tr>
		        <th colspan="11">BULAN {{$month}} {{$year}}</th>
		    </tr>
		    <tr>
		        <th colspan="11">KEGIATAN YANG {{$status}}</th>
		    </tr>
		    <tr>
		        <th colspan="11"></th>
		    </tr>
	        <tr>
	            <th>NO</th>
	            <th>KEGIATAN</th>
	            <th>K/L/D/I</th>
	            <th>JENIS PEKERJAAN/PROYEK</th>
	            <th>PAGU ANGGARAN</th>
	            <th>SUMBER DANA APBN/APBD</th>
	            <th>TAHAPAN PROYEK/PEKERJAAN</th>
	            <th>TAHAPAN PENGAWALAN &amp; PENGAMANAN</th>
	            <th>NO &amp; TANGGAL SURAT PERINTAH</th>
	            <th>PENYERAPAN ANGGARAN (%)</th>
	            <th>KETERANGAN</th>
			</tr>
	    </thead>
	    <tbody>
			@php
			$no = 1;
			@endphp
			@if(count($data)>0)
				@foreach($data as $row)
				<tr>
				    <td>{{$no++}}</td>
				    <td>{{$row->nama_kegiatan}}</td>
				    <td>{{$row->instansi_pemohon}}</td>
				    <td>{{$row->jenis_pekerjaan}}</td>
				    <td>Rp {{number_format($row->pagu_anggaran, 0, '', '.')}}</td>
				    <td>{{$row->sumber_dana}}</td>
				    <td>{{$row->tahapan_proyek}}</td>
				    <td>{{$row->tahapan_pengawalan}}</td>
				    <td>{{$row->nomor_surat_perintah}}</td>
				    <td>{{$row->penyerapan_anggaran}}</td>
				    <td>{{$row->keterangan}}</td>
				</tr>
				@endforeach
			@else
				<tr class="not">
					<td colspan="11">DATA TIDAK TERSEDIA</td>
				</tr>
			@endif
	    </tbody>
	</table>
</body>
</html>