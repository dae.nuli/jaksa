@extends('admin.layouts.app')

@section('head-script')
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.3/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('end-script')
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      minDate: 0,
      startDate: '-0m',
    });

    $( ".status" ).wrapAll( "<div class='col-sm-8'></div>");
    $( ".status" ).find('[name="status"]').css('margin-left','0');

  </script>
@endsection

@section('content')
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('success')}}
  </div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection