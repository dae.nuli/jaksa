<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Laporan Kegiatan - {{$month}}/{{$year}}</title>
    <link rel="stylesheet" href="{{asset('assets/css/pdf.css')}}" media="all" />
  </head>
  <body>
  	<h5 class="title">TABEL LAPORAN KEGIATAN</h5>
  	<h5 class="title">TIM PENGAWALAN DAN PENGAMANAN PEMERINTAH DAN PEMBANGUNAN (TP4)</h5>
  	<h5 class="title">KEJAKSAAN NEGERI NUNUKAN</h5>
  	<h5 class="title">BULAN {{$month}} {{$year}}</h5>
  	<hr>
  	<div>
  		<table>
  			<tr>
  				<td>A. KEGIATAN YANG {{$status}}</td>
  			</tr>
  		</table>
  	</div>
  	<div>
  		<table class="table-content">
  			<thead>
	  			<tr class="tr-atas" style="background-color: darkgray;">
		            <th>NO</th>
		            <th>KEGIATAN</th>
		            <th>K/L/D/I</th>
		            <th>JENIS PEKERJAAN/PROYEK</th>
		            <th>PAGU ANGGARAN</th>
		            <th>SUMBER DANA APBN/APBD</th>
		            <th>TAHAPAN PROYEK/PEKERJAAN</th>
		            <th>TAHAPAN PENGAWALAN &amp; PENGAMANAN</th>
		            <th>NO &amp; TANGGAL SURAT PERINTAH</th>
		            <th>PENYERAPAN ANGGARAN (%)</th>
		            <th>KETERANGAN</th>
		         </tr>
  			</thead>
  			<tbody>
  				@php
  				$no = 1;
  				@endphp
  				@if(count($data)>0)
					@foreach($data as $row)
			        <tr>
			            <td>{{$no++}}</td>
			            <td>{{$row->nama_kegiatan}}</td>
			            <td>{{$row->instansi_pemohon}}</td>
			            <td>{{$row->jenis_pekerjaan}}</td>
			            <td>Rp {{number_format($row->pagu_anggaran, 0, '', '.')}}</td>
			            <td>{{$row->sumber_dana}}</td>
			            <td>{{$row->tahapan_proyek}}</td>
			            <td>{{$row->tahapan_pengawalan}}</td>
			            <td>{{$row->nomor_surat_perintah}}</td>
			            <td>{{$row->penyerapan_anggaran}}</td>
			            <td>{{$row->keterangan}}</td>
			        </tr>
			        @endforeach
		        @else
			        <tr class="not">
			        	<td colspan="11">DATA TIDAK TERSEDIA</td>
			        </tr>
		        @endif
	    	</tbody>
  		</table>
  	</div>
  </body>
</html>