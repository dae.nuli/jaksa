@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.3/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.3/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
  	<script src="{{ asset('AdminLTE-2.4.3/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="{{asset('AdminLTE-2.4.3/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.3/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.3/dist/js/custom.js')}}"></script>
	<script type="text/javascript">
    	$('.select2').css('width', '100%').select2();
		var table;
		$(function() {
		    table = $('#example2').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!!$ajax!!}',
        		order: [[6,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
		            { data: 'user_id', searchable: false, orderable: false},
		            // { data: 'instansi_pemohon', searchable: false, orderable: false},
		            { data: 'nama_kegiatan', searchable: false, orderable: false},
		            { data: 'pagu_anggaran', searchable: false, orderable: false},
		            { data: 'keterangan', searchable: false, orderable: false},
		            { data: 'status', searchable: false, orderable: false},
		            { data: 'created_at', searchable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1; 
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('success')}}
  </div>
@endif
<div class="box">
	<div class="box-header with-border">
	  	{{-- <h3 class="box-title">Title</h3> --}}
	  	@if(auth('admin')->user()->can('kegiatan_create'))
	        <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a>
        @endif
        <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModalTwo" style="margin-left: 5px;"><i class="fa fa-fw fa-sliders"></i> Filter</a> 
        <a href="#" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-download"></i> Download</a>
{{-- 
	  	<div class="box-tools pull-right">
	    	<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    	<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
	  	</div> --}}
	</div>
	<div class="box-body">
	  	<table id="example2" class="table table-bordered table-hover">
            <thead>
	            <tr>
	              <th>#</th>
	              {{-- <th>User</th> --}}
	              <th>Instansi</th>
	              <th>Nama Kegiatan</th>
	              <th>Pagu Anggaran</th>
	              <th>Keterangan</th>
	              <th>Status</th>
	              <th>Date</th>
	              <th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter Download</h4>
			</div>
			<div class="modal-body">
				<form action="{{url('admin/kegiatan/download')}}" id="form1" method="POST" class="form-horizontal">
					@csrf
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Year</label>

	                  <div class="col-sm-10">
	                    <select class="form-control select2" name="year" required>
	                    	@foreach(range(2017, date('Y')) as $row)
		                    	<option value="{{$row}}" {{($row==date('Y')) ? 'selected': ''}}>{{ $row }}</option>
	                    	@endforeach
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Month</label>

	                  <div class="col-sm-10">
	                    <select class="select2 form-control" name="month" required>
	                    	@for($i=1; $i <= 12; $i++)
		                    	<option value="{{$i}}" {{($i==date('n')) ? 'selected': ''}}>{{ date('F', strtotime(date('Y').'-'.$i.'-01')) }}</option>
	                    	@endfor
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Status</label>

	                  <div class="col-sm-10">
	                    <select class="form-control select2" name="status" required>
	                    	<option value="1">DITERIMA</option>
	                    	<option value="2">DITOLAK</option>
	                    	<option value="3">SEDANG PROSES</option>
	                    	<option value="4">SELESAI</option>
	                    	<option value="0">BARU</option>
	                    </select>
	                  </div>
	                </div>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Type</label>
	                  <div class="col-sm-10">
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="document_type" id="document_type" value="1" checked="">
		                      PDF
		                    </label>
		                </div>
	                    <div class="radio">
		                    <label>
		                      <input type="radio" name="document_type" id="document_type" value="2">
		                      Excel
		                    </label>
		                </div>
	                  </div>
	                </div>
	              </div>
	              <!-- /.box-footer -->
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form1" class="btn btn-primary">Download</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="myModalTwo">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter Kegiatan</h4>
			</div>
			<div class="modal-body">
				<form action="{{url('admin/kegiatan')}}" id="form2" method="GET" class="form-horizontal">
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Status</label>

	                  <div class="col-sm-10">
	                    <select class="form-control" name="status">
	                    	<option value="">- Semua -</option>
	                    	<option value="3" {{($status == '3') ? 'selected' : ''}}>SEDANG PROSES</option>
	                    	<option value="1" {{($status == '1') ? 'selected' : ''}}>DITERIMA</option>
	                    	<option value="2" {{($status == '2') ? 'selected' : ''}}>DITOLAK</option>
	                    	<option value="4" {{($status == '4') ? 'selected' : ''}}>SELESAI</option>
	                    	<option value="0" {{($status == '0') ? 'selected' : ''}}>BARU</option>
	                    </select>
	                  </div>
	                </div>
	              </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form2" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div>
@endsection




