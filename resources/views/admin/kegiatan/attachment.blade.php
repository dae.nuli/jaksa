<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
        <p class="uploaded_image">
        	@foreach($options['value'] as $id => $row)
		        <a href="{{url('admin/kegiatan/viewFile/'.$id)}}" target="_blank" class="btn-link" title="">{{$row}} (<i class="fa fa-fw fa-download"></i>)</a> <br>
        	@endforeach
	    </p>
  </div>
</div>