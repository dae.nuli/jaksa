@extends('admin.layouts.app')

@section('head-script')
  <!-- bootstrap wysihtml5 - text editor -->
  {{-- <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.3/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}"> --}}
@endsection

@section('end-script')
  <!-- CK Editor -->
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/ckeditor/ckeditor.js')}}"></script>
  <!-- Bootstrap WYSIHTML5 -->
  {{-- <script src="{{ asset('AdminLTE-2.4.3/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script> --}}
  <!-- Select2 -->
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
    $( ".status" ).wrapAll( "<div class='col-sm-8'></div>");
    $( ".status" ).find('[name="status"]').css('margin-left','0');
    // $('.textarea').wysihtml5();
    // $('.textarea').css({
    //   'width': '100%',
    //   'height': '200px',
    //   'font-size': '14px',
    //   'line-height': '18px',
    //   'border': '1px solid #dddddd',
    //   'padding': '10px'
    // });
    CKEDITOR.replace('textarea')
  </script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection