@extends('admin.layouts.app')

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.3/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{asset('AdminLTE-2.4.3/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.3/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.3/dist/js/custom.js')}}"></script>
	<script type="text/javascript">
		var table;
		$(function() {
		    table = $('#example2').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{{$ajax}}',
        		order: [[5,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
		            { data: 'title', searchable: false, orderable: false},
		            { data: 'content', searchable: false, orderable: false},
		            { data: 'file', searchable: false, orderable: false},
		            { data: 'status', searchable: false, orderable: false},
		            { data: 'created_at', searchable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1; 
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
	  	{{-- <h3 class="box-title">Title</h3> --}}
	  	@if(auth('admin')->user()->can('undang_create'))
	        <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a>
        @endif
{{-- 
	  	<div class="box-tools pull-right">
	    	<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    	<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
	  	</div> --}}
	</div>
	<div class="box-body">
	  	<table id="example2" class="table table-bordered table-hover">
            <thead>
	            <tr>
	              <th>#</th>
	              <th>Judul</th>
	              <th>Deskripsi</th>
	              <th>File</th>
	              <th>Status</th>
	              <th>Date</th>
	              <th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>
@endsection