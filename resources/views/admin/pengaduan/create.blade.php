@extends('admin.layouts.app')

@section('head-script')
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.3/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.3/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('end-script')
  <!-- Select2 -->
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.priceformat.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.multifile.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
    $('.select2').select2();
    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      minDate: 0,
      startDate: '-0m',
    })
    $('.number').priceFormat({
      prefix: '',
      thousandsSeparator: '.',
      clearPrefix:true,
      centsLimit: 0,
      // centsSeparator: '.',
      // clearOnEmpty: true
    });
    $( ".status" ).wrapAll( "<div class='col-sm-8'></div>");
    $( ".status" ).find('[name="status"]').css('margin-left','0');

    $('#multifile').multifile();
    $(document).on('click','.delete-file',function(e){
      e.preventDefault();
      var CSRF_TOKEN = $('input[name="_token"]').attr('value');
      var METHOD = $('input[name="_method"]').attr('value');
      var url = $(this).data('url');
      var $this = $(this);

      if(confirm('Are you sure want to delete ?')) {
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          success : function(data){
            if(data.msg) {
              // console.log('h');
              $this.next().next().remove();
              $this.next().remove();
              $this.remove();
            }
          }
        }).always(function(data) {
            // $('#dataTableBuilder').DataTable().draw(false);
        });
        
      } else {

      }
    });
  </script>
@endsection

@section('content')
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('success')}}
  </div>
@endif
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection