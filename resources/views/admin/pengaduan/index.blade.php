@extends('admin.layouts.app')

@section('head-script')
	<link rel="stylesheet" href="{{asset('AdminLTE-2.4.3/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('end-script')
	<script src="{{asset('AdminLTE-2.4.3/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.3/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('AdminLTE-2.4.3/dist/js/custom.js')}}"></script>
	<script type="text/javascript">
		var table;
		$(function() {
		    table = $('#example2').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!!$ajax!!}',
        		order: [[5,'desc']],
		        columns: [
		            { data: 'id', searchable: false, orderable: false},
		            { data: 'user_id', searchable: false, orderable: false},
		            { data: 'nama_pengadu', searchable: false, orderable: false},
		            { data: 'nama_kegiatan', searchable: false, orderable: false},
		            { data: 'status', searchable: false, orderable: false},
		            { data: 'created_at', searchable: false},
		            { data: 'action', searchable: false, orderable: false}
		        ],
		        columnDefs: [{
		          "targets": 0,
		          "searchable": false,
		          "orderable": false,
		          "data": null,
		          // "title": 'No.',
		          "render": function (data, type, full, meta) {
		              return meta.settings._iDisplayStart + meta.row + 1; 
		          }
		        }],
		    });
		});
	</script>
@endsection

@section('content')
@if(Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('success')}}
  </div>
@endif
<div class="box">
	<div class="box-header with-border">
	  	@if(auth('admin')->user()->can('pengaduan_create'))
	        {{-- <a href="{{$create}}" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Create</a> --}}
        @endif
        <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModalTwo"><i class="fa fa-fw fa-sliders"></i> Filter</a> 
	</div>
	<div class="box-body">
	  	<table id="example2" class="table table-bordered table-hover">
            <thead>
	            <tr>
	              <th>#</th>
	              <th>User</th>
	              <th>Pengadu</th>
	              <th>Nama Kegiatan</th>
	              <th>Status</th>
	              <th>Date</th>
	              <th>Action</th>
	            </tr>
            </thead>
            <tbody>
	        </tbody>
	    </table>
	</div>
</div>

<div class="modal fade" id="myModalTwo">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Filter Kegiatan</h4>
			</div>
			<div class="modal-body">
				<form action="{{url('admin/pengaduan')}}" id="form2" method="GET" class="form-horizontal">
	              <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Status</label>

	                  <div class="col-sm-10">
	                    <select class="form-control" name="status">
	                    	<option value="">- Semua -</option>
	                    	<option value="3" {{($status == '3') ? 'selected' : ''}}>SEDANG PROSES</option>
	                    	<option value="1" {{($status == '1') ? 'selected' : ''}}>DITERIMA</option>
	                    	<option value="2" {{($status == '2') ? 'selected' : ''}}>DITOLAK</option>
	                    	<option value="4" {{($status == '4') ? 'selected' : ''}}>SELESAI</option>
	                    	<option value="0" {{($status == '0') ? 'selected' : ''}}>BARU</option>
	                    </select>
	                  </div>
	                </div>
	              </div>
	            </form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
				<button type="submit" form="form2" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div>
@endsection