<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
      	<input class="form-control multifile" name="attach[]" type="file" id="multifile">
        <p class="uploaded_image">
        	@foreach($options['value'] as $id => $row)
		        <a href="#" data-url="{{url('admin/foto/deleteFile/'.$id)}}" class="multifile_remove_input delete-file"><i class="fa fa-fw fa-times"></i></a>
		        <a href="{{url('admin/foto/viewFile/'.$id)}}" target="_blank" class="btn-link" title="">{{$row}} (<i class="fa fa-fw fa-download"></i>)</a> <br>
        	@endforeach
	    </p>
  </div>
</div>