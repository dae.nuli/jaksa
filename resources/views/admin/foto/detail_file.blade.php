<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
        	@foreach($options['value'] as $id => $row)
		        <a href="{{url('admin/foto/viewFile/'.$id)}}" target="_blank" class="btn-link">
			        <img src="{{asset('store/foto/'.$row)}}" width="200" class="image">
			    </a>
        	@endforeach
  </div>
</div>