@extends('admin.layouts.app')

@section('content')
<div class="box">
  <div class="box-body">
      <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Status</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
          </tbody>
      </table>
  </div>
</div>
@endsection