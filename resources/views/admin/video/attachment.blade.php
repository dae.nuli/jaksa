<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
      {{-- <input class="form-control" name="attachment" type="file" id="imgInp"> --}}
      <a href="{{ asset('store/video/'.$options['value']) }}" data-sub-html="Image" target="_blank">
          <img class="img-responsive thumbnail" width="200" src="{{ asset('store/video/'.$options['value']) }}">
      </a>
  </div>
</div>