@extends('admin.layouts.app')

@section('head-script')
	<style type="text/css">
		.image {
			border: 1px solid black;
			border-radius: 3px;
			margin: 0 10px 10px 0;
		}
	</style>
@endsection

@section('end-script')
  <!-- Select2 -->
  <script type="text/javascript">
    $( ".status" ).wrapAll( "<div class='col-sm-8'></div>");
    $( "[name='status']" ).attr('disabled', true);
    $( ".status" ).find('[name="status"]').css('margin-left','0');
  </script>
@endsection

@section('content')
<div class="box">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
	<div class="box-body">
	  	{!! form_rest($form) !!}
	</div>
	<div class="box-footer">
		<div class="col-sm-8 col-sm-offset-2">
		</div>
	</div>
    {!! form_end($form) !!}
</div>
@endsection