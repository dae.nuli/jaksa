<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
    <a href="{{url('admin/kerjaNyata/viewFile/'.$options['value']['id'])}}" target="_blank" class="btn-link">
        <img src="{{asset('store/kerja/'.$options['value']['file'])}}" width="200" class="image">
    </a>
  </div>
</div>