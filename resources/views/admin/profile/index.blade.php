@extends('admin.layouts.app')

@section('end-script')
  <!-- Select2 -->
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
  </script>
@endsection

@section('content')
@if(Session::has('profile'))
  <div class="alert alert-success alert-dismissible">
    {{Session::get('profile')}}.
  </div>
@endif
<div class="box">
  <div class="box-header with-border">
  </div>
    {!! form_start($form, ['class' => 'form-horizontal']) !!}
  <div class="box-body">
      {!! form_rest($form) !!}
  </div>
  <div class="box-footer">
    <div class="col-sm-8 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
    {!! form_end($form) !!}
</div>
@endsection