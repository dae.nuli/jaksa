<div class="form-group">
  <label for="attachment" class="col-sm-2 control-label">File</label>
  <div class="col-sm-8">
  	<input class="form-control" name="attach" type="file">
    <a href="{{url('admin/slider/viewFile/'.$options['value']['id'])}}" target="_blank" class="btn-link">
      <img src="{{asset('store/slider/'.$options['value']['file'])}}" width="200" class="image">
    </a>
  </div>
</div>