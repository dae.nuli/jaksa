@php
$uri = request()->segment(2);
@endphp

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('assets/images/logojaksa.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::guard('admin')->user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li {{($uri == 'home') ? 'class=active' : ''}}>
        <a href="{{url('admin/home')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      @if(auth('admin')->user()->can('kegiatan_index'))
      <li {{($uri == 'kegiatan') ? 'class=active' : ''}}>
        <a href="{{url('admin/kegiatan')}}">
          <i class="fa fa-calendar-check-o"></i> <span>Kegiatan</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('pengaduan_index'))
      <li {{($uri == 'pengaduan') ? 'class=active' : ''}}>
        <a href="{{url('admin/pengaduan')}}">
          <i class="fa fa-bullhorn"></i> <span>Pengaduan</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('foto_index'))
      <li {{($uri == 'foto') ? 'class=active' : ''}}>
        <a href="{{url('admin/foto')}}">
          <i class="fa fa-photo"></i> <span>Foto</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('slider_index'))
      <li {{($uri == 'slider') ? 'class=active' : ''}}>
        <a href="{{url('admin/slider')}}">
          <i class="fa fa-photo"></i> <span>Slider</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('kerja_index'))
      <li {{($uri == 'kerjaNyata') ? 'class=active' : ''}}>
        <a href="{{url('admin/kerjaNyata')}}">
          <i class="fa fa-photo"></i> <span>Kerja Nyata</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('video_index'))
      <li {{($uri == 'video') ? 'class=active' : ''}}>
        <a href="{{url('admin/video')}}">
          <i class="fa fa-video-camera"></i> <span>Video</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('undang_index'))
      <li {{($uri == 'uu') ? 'class=active' : ''}}>
        <a href="{{url('admin/uu')}}">
          <i class="fa fa-file-pdf-o"></i> <span>UU</span>
        </a>
      </li>
      @endif

      @if(auth('admin')->user()->can('user_index'))
      <li {{($uri == 'users') ? 'class=active' : ''}}>
        <a href="{{url('admin/users')}}">
          <i class="fa fa-user"></i> <span>User</span>
        </a>
      </li>
      @endif

 {{--      @if(auth('admin')->user()->can('visitor_index'))
      <li {{($uri == 'visitor') ? 'class=active' : ''}}>
        <a href="{{url('admin/visitor')}}">
          <i class="fa fa-signal"></i> <span>Pengunjung</span>
        </a>
      </li>
      @endif --}}

      @if(auth('admin')->user()->can('visitor_index'))
        <li class="treeview {{($uri == 'visitorSummary' || $uri == 'visitorAll' || $uri == 'visitorVisits' || $uri == 'visitorBots' || $uri == 'visitorCountries' || $uri == 'visitorOs' || $uri == 'visitorBrowsers' || $uri == 'visitorLanguages' || $uri == 'visitorUnique' || $uri == 'visitorUsers' || $uri == 'visitorUrls') ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-signal"></i> <span>Pengunjung</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li {{($uri == 'visitorSummary') ? 'class=active' : ''}}><a href="{{url('admin/visitorSummary')}}"><i class="fa fa-circle-o"></i> Summary</a></li>
            <li {{($uri == 'visitorAll') ? 'class=active' : ''}}><a href="{{url('admin/visitorAll')}}"><i class="fa fa-circle-o"></i> All</a></li>
            <li {{($uri == 'visitorVisits') ? 'class=active' : ''}}><a href="{{url('admin/visitorVisits')}}"><i class="fa fa-circle-o"></i> Visits</a></li>
            <li {{($uri == 'visitorBots') ? 'class=active' : ''}}><a href="{{url('admin/visitorBots')}}"><i class="fa fa-circle-o"></i> Bots</a></li>
            <li {{($uri == 'visitorCountries') ? 'class=active' : ''}}><a href="{{url('admin/visitorCountries')}}"><i class="fa fa-circle-o"></i> Country</a></li>
            <li {{($uri == 'visitorOs') ? 'class=active' : ''}}><a href="{{url('admin/visitorOs')}}"><i class="fa fa-circle-o"></i> OS</a></li>
            <li {{($uri == 'visitorBrowsers') ? 'class=active' : ''}}><a href="{{url('admin/visitorBrowsers')}}"><i class="fa fa-circle-o"></i> Browsers</a></li>
            <li {{($uri == 'visitorLanguages') ? 'class=active' : ''}}><a href="{{url('admin/visitorLanguages')}}"><i class="fa fa-circle-o"></i> Languages</a></li>
            <li {{($uri == 'visitorUnique') ? 'class=active' : ''}}><a href="{{url('admin/visitorUnique')}}"><i class="fa fa-circle-o"></i> Unique</a></li>
            <li {{($uri == 'visitorUsers') ? 'class=active' : ''}}><a href="{{url('admin/visitorUsers')}}"><i class="fa fa-circle-o"></i> Users</a></li>
            <li {{($uri == 'visitorUrls') ? 'class=active' : ''}}><a href="{{url('admin/visitorUrls')}}"><i class="fa fa-circle-o"></i> Url</a></li>
          </ul>
        </li>
      @endif
{{--       <li {{($uri == 'admin') ? 'class=active' : ''}}>
        <a href="{{url('admin/admin')}}">
          <i class="fa fa-users"></i> <span>Admin</span>
        </a>
      </li> --}}
      <li class="header">Pengaturan</li>
        <li class="treeview {{($uri == 'role' || $uri == 'permission' || $uri == 'rolePermission' || $uri == 'admin') ? 'active' : ''}}">
          <a href="#">
            @if(auth('admin')->user()->can('admin_index') && auth('admin')->user()->can('role_index') && auth('admin')->user()->can('permission_index'))
            <i class="fa fa-laptop"></i>
            <span>Permission Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            @endif
          </a>
          <ul class="treeview-menu">
            @if(auth('admin')->user()->can('admin_index'))
            <li {{($uri == 'admin') ? 'class=active' : ''}}><a href="{{url('admin/admin')}}"><i class="fa fa-circle-o"></i> Admin</a></li>
            @endif

            @if(auth('admin')->user()->can('role_index'))
            <li {{($uri == 'role') ? 'class=active' : ''}}><a href="{{url('admin/role')}}"><i class="fa fa-circle-o"></i> Role</a></li>
            @endif

            @if(auth('admin')->user()->can('permission_index'))
            <li {{($uri == 'permission') ? 'class=active' : ''}}><a href="{{url('admin/permission')}}"><i class="fa fa-circle-o"></i> Permission</a></li>
            @endif
            {{-- <li {{($uri == 'rolePermission') ? 'class=active' : ''}}><a href="{{url('admin/rolePermission')}}"><i class="fa fa-circle-o"></i> Role Permission</a></li> --}}
          </ul>
        </li>
      @if(auth('admin')->user()->can('contact_index'))
      <li {{($uri == 'contact') ? 'class=active' : ''}}>
        <a href="{{url('admin/contact')}}">
          <i class="fa fa-envelope"></i> <span>Contact</span>
        </a>
      </li>
      @endif
      <li {{($uri == 'profile') ? 'class=active' : ''}}>
        <a href="{{url('admin/profile')}}">
          <i class="fa fa-circle-o text-red"></i> <span>Profil</span>
        </a>
      </li>
      {{-- <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li> --}}
    </ul>

    </section>
    <!-- /.sidebar -->
  </aside>