
@extends('admin.layouts.app')

@section('content')
      <div class="error-page">
        <h2 class="headline text-red">500</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Access Denied.</h3>

          <p>
            You do not have permission to access this page.
            Meanwhile, you may <a href="{{url('admin/home')}}">return to dashboard</a> or try using the search form.
          </p>

        </div>
      </div>
@endsection