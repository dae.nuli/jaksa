@extends('layouts.app')

@section('end-script')
    <script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
    <script type="text/javascript">
    $.validate({
        onSuccess : function() {
            $.blockUI({ 
                message: '<h1 style="font-weight:300">Please Wait</h1>',
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#fff', 
                    '-webkit-border-radius': '0', 
                    '-moz-border-radius': '0',  
                    color: '#000',
                    'text-transform': 'uppercase'
                },
                baseZ:9999
            });
        }
    });
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(Session::has('success'))
            <div class="alert alert-success">
                <i class="icon-gift"></i>{{Session::get('success')}}
            </div>
            @endif
            
            <div class="card">
                <div class="card-header">{{ __('Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('profile') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" disabled="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" disabled="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
