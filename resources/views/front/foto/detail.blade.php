@extends('front.layouts.app')

@section('end-script')
<script type="text/javascript">

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tp4d-kejari-nunukan.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

@section('content-top')


				<div class="container clearfix">

					<!-- Portfolio Single Gallery
					============================================= -->
					<div class="col_two_third portfolio-single-image nobottommargin">
						<div class="fslider" data-arrows="false" data-thumbs="true" data-animation="fade">
							<div class="flexslider">
								<div class="slider-wrap">
									@foreach($detailfoto as $row)
									<div class="slide" data-thumb="{{asset('store/foto/'.$row->file)}}"><a href="#"><img src="{{asset('store/foto/'.$row->file)}}" alt=""></a></div>
									@endforeach
								</div>
							</div>
						</div>
					</div><!-- .portfolio-single-image end -->

					<!-- Portfolio Single Content
					============================================= -->
					<div class="col_one_third portfolio-single-content col_last nobottommargin">

						<!-- Portfolio Single - Description
						============================================= -->
						<div class="fancy-title title-bottom-border">
							<h2>{{$detail->title}}</h2>
						</div>
						<p>{{$detail->content}}</p>

						<div class="line"></div>

						<!-- Portfolio Single - Meta
						============================================= -->
						<ul class="portfolio-meta bottommargin">
							<li><span><i class="icon-user"></i>Created by:</span> {{$detail->admin->name}}</li>
							<li><span><i class="icon-calendar3"></i>Completed on:</span> {{date('d F Y', strtotime($detail->created_at))}}</li>
						</ul>

					</div><!-- .portfolio-single-content end -->

					<div class="clear"></div>

					<div class="divider divider-center"><i class="icon-circle"></i></div>

					<!-- Related Portfolio Items
					============================================= -->
					<h4>Related Foto:</h4>

					<div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="30" data-nav="false" data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">
					@foreach($related as $photo)
						<div class="oc-item">
							<div class="iportfolio">
								<div class="portfolio-image">
									<a href="portfolio-single-gallery.html">
										<img src="{{asset('store/foto/'.$photo->oneFoto()->file)}}" alt="Shake It!">
									</a>
									<div class="portfolio-overlay" data-lightbox="gallery">
										@foreach($photo->detail as $key => $val)
											@if($key == 0)
											<a href="{{asset('store/foto/'.$val->file)}}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
											@else
											<a href="{{asset('store/foto/'.$val->file)}}" class="hidden" data-lightbox="gallery-item"></a>
											@endif
										@endforeach
										<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h3>
								</div>
							</div>
						</div>
					@endforeach

					</div><!-- .portfolio-carousel end -->

				</div>


{{-- <div class="container clearfix">
	<div class="single-post nobottommargin">
		<div class="entry clearfix">

			<div class="entry-title">
				<h2>{{$detail->title}}</h2>
			</div>
			<ul class="entry-meta clearfix">
				<li><i class="icon-calendar3"></i> {{date('d F Y', strtotime($detail->created_at))}}</li>
				<li><a href="#"><i class="icon-user"></i> {{$detail->admin->name}}</a></li>
				<li><i class="icon-folder-open"></i> <a href="#">Media</a></li>
				<li><a href="#"><i class="icon-picture"></i></a></li>
			</ul>
			<div class="entry-image bottommargin">
				<a href="#">
					<img src="{{asset('store/foto/'.$detail->file)}}" alt="{{$detail->title}}">
				</a>
			</div>
			<div class="entry-content notopmargin">
				<p>{{$detail->content}}</p>
			</div>
		</div>

		<div class="line"></div>

		<h4>Related Foto:</h4>

		<div class="related-posts clearfix">

			<div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">

				@foreach($foto as $row)
				<div class="entry clearfix">
					<div class="entry-image">
						<a href="#"><img src="{{asset('store/foto/'.$row->file)}}" alt="{{$row->title}}"></a>
					</div>
					<div class="entry-title">
						<h2><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h2>
					</div>
					<ul class="entry-meta clearfix">
						<li><i class="icon-calendar3"></i> {{date('d M Y', strtotime($row->created_at))}}</li>
						<li><a href="#"><i class="icon-picture"></i></a></li>
					</ul>
					<div class="entry-content">
						<p>{{Str::words($row->content, 20)}}</p>
						<a href="{{url('foto/'.$row->id)}}" class="more-link">Read More</a>
					</div>
				</div>
				@endforeach
			</div>

		</div>

	</div>
	<div id="disqus_thread"></div>
</div> --}}
@endsection