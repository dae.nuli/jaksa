@extends('front.layouts.app')

@section('end-script')
<script type="text/javascript">

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tp4d-kejari-nunukan.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

@section('content-top')
				{{-- <div class="container clearfix">
					<div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">

						@foreach($foto as $row)
						<div class="entry clearfix">
							<div class="entry-image">
								<a href="{{asset('store/foto/'.$row->file)}}" data-lightbox="image"><img class="image_fade" src="{{asset('store/foto/'.$row->file)}}" alt="{{$row->title}}"></a>
							</div>
							<div class="entry-title">
								<h2><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h2>
							</div>
							<ul class="entry-meta clearfix">
								<li><i class="icon-calendar3"></i> {{date('d M Y', strtotime($row->created_at))}}</li>
								<li><a href="#"><i class="icon-picture"></i></a></li>
							</ul>
							<div class="entry-content">
								<p>{{Str::words($row->content, 20)}}</p>
								<a href="{{url('foto/'.$row->id)}}" class="more-link">Read More</a>
							</div>
						</div>
						@endforeach
					</div>
					<div>
						{{$foto->links()}}
					</div>
					<div id="disqus_thread"></div>
				</div> --}}
<div class="container clearfix">
	<div id="portfolio" class="portfolio grid-container clearfix">
		@foreach($foto as $row)
		<article class="portfolio-item">
			<div class="portfolio-image">
				<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
					<div class="flexslider">
						<div class="slider-wrap">
							@foreach($row->detail as $val)
							<div class="slide"><a href="{{url('foto/'.$row->id)}}"><img src="{{asset('store/foto/'.$val->file)}}" alt="{{$row->title}}"></a></div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="portfolio-overlay" data-lightbox="gallery">
					@foreach($row->detail as $key => $val)
						@if($key == 0)
						<a href="{{asset('store/foto/'.$val->file)}}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
						@else
						<a href="{{asset('store/foto/'.$val->file)}}" class="hidden" data-lightbox="gallery-item"></a>
						@endif
					@endforeach
					<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc">
				<h3><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h3>
			</div>
		</article>
		@endforeach
	</div>
	<div>
		{{$foto->links()}}
	</div>
	<div id="disqus_thread"></div>
</div>
@endsection