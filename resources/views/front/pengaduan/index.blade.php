@extends('front.layouts.app')

@section('head-script')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
	<style type="text/css">
		.uploaded_image {
		    margin-bottom: 0 !important;
		}
	</style>
@endsection

@section('end-script')
	{{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
	<!-- Select2 -->
	<script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
	<script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
	<script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.priceformat.min.js') }}"></script>
	<script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
	<script src="{{ asset('assets/js/jquery.multifile.js') }}"></script>
	<script type="text/javascript">
	$.validate({
	    onSuccess : function() {
	      waiting();
	    }
	});
	$('.number').priceFormat({
		prefix: '',
		thousandsSeparator: '.',
		clearPrefix:true,
		centsLimit: 0,
		// centsSeparator: '.',
		// clearOnEmpty: true
	});
	$('#multifile').multifile();
	</script>
@endsection

@section('content-top')
<div class="container clearfix">
	<div class="row justify-content-center">
		<div class="col-md-10">
			@if(Session::has('status'))
			<div class="alert alert-success">
			  	<i class="icon-gift"></i>{{Session::get('status')}}
			</div>
			@endif
			@if(Session::has('file_required'))
			<div class="alert alert-danger">
			  	<i class="icon-gift"></i>{{Session::get('file_required')}}
			</div>
			@endif
			
			@if ($errors->any())
			<div class="style-msg2 errormsg">
					<div class="msgtitle">Fix the Following Errors:</div>
					<div class="sb-msg">
						<ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
						</ul>
					</div>
				</div>
			@endif
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-10">
			<form action="{{url('pengaduan')}}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group row">
					<label for="pengadu" class="col-md-4 col-form-label text-md-right">Nama Pengadu</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="pengadu" data-validation="required" value="{{old('nama_pengadu')}}" name="nama_pengadu">
					</div>
				</div>
				<div class="form-group row">
					<label for="nama_kegiatan" class="col-md-4 col-form-label text-md-right">Nama Kegiatan</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="nama_kegiatan" data-validation="required" value="{{old('nama_kegiatan')}}" name="nama_kegiatan">
					</div>
				</div>
				<div class="form-group row">
					<label for="penanggung_jawab" class="col-md-4 col-form-label text-md-right">Penanggung Jawab</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="penanggung_jawab" data-validation="required" value="{{old('penanggungjawab')}}" name="penanggungjawab">
					</div>
				</div>
				<div class="form-group row">
					<label for="telp" class="col-md-4 col-form-label text-md-right">Telp</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="telp" data-validation="required" value="{{old('phone')}}" name="phone">
					</div>
				</div>
				<div class="form-group row">
					<label for="permasalahan" class="col-md-4 col-form-label text-md-right">Permasalahan</label>
					<div class="col-md-7">
						<textarea class="form-control" id="permasalahan" minlength="150" rows="5" data-validation="required" name="permasalahan">{{old('permasalahan')}}</textarea>
					</div>
				</div>
				<div class="form-group row">
					<label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
					<div class="col-md-7">
						<input type="email" class="form-control" id="email" data-validation="required" value="{{old('email')}}" name="email">
					</div>
				</div>
				<div class="form-group row">
					<label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
					<div class="col-md-7">
						<input type="password" class="form-control" id="password" data-validation="required" value="{{old('password')}}" name="password">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right">Dokumentasi Kegiatan</label>
					<div class="col-md-7">
						<input type="file" class="form-control multifile" id="multifile" name="attach[]">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right"></label>
					<div class="col-md-7">
						{!! captcha_img('flat') !!}
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right">Captcha</label>
					<div class="col-md-7">
						<input type="text" class="form-control"  name="captcha">
					{{-- {!! app('captcha')->display() !!} --}}
					</div>
				</div>
				{{-- <div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right"></label>
					<div class="col-md-7">
					{!! app('captcha')->display() !!}
					</div>
				</div> --}}
				<div class="form-group row">
					<div class="col-md-7 offset-md-4">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection