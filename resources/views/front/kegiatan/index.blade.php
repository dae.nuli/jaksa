@extends('front.layouts.app')

@section('content-top')
<div class="container clearfix">
	<div class="col_full">
		
{{-- 	<div class="row justify-content-center">
		<div class="col-md-10"> --}}
			<table class="table table-striped">
				<thead>
					<tr>
					  	<th>No</th>
					  	<th>Pemohon</th>
					  	<th>Kegiatan</th>
					  	<th>Anggaran</th>
					  	<th>Mulai SPMK</th>
					  	<th>Berakhir SPMK</th>
					  	<th>Masa Pelaksanaan</th>
					  	<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
					@foreach($kegiatan as $key => $row)
					<tr>
                        <th>{{($kegiatan->currentPage()-1) * $kegiatan->perPage() + $key + 1}}</th>
					  	<td>{{$row->user->name}}</td>
					  	{{-- <td>{{$row->instansi_pemohon}}</td> --}}
					  	<td>{{$row->nama_kegiatan}}</td>
					  	<td>Rp {{number_format($row->pagu_anggaran, 0, '', '.')}}</td>
					  	<td>{{$row->mulai_spmk}}</td>
					  	<td>{{$row->berakhir_spmk}}</td>
					  	<td>{{$row->masa_pelaksanaan}}</td>
					  	<td>{{$row->keterangan}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
            {{$kegiatan->links()}}
		{{-- </div>
	</div> --}}
	</div>
</div>
@endsection