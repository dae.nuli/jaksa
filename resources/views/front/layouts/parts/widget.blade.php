<div class="footer-widgets-wrap clearfix">

	<div class="col_half">

		<div class="widget clearfix">

			{{-- <img src="{{asset('assets/images/footer-widget-logo.png')}}" alt="" class="footer-logo"> --}}

						<p>Sistem Informasi TP4D Kejaksaan Negeri Sukoharjo</p>
{{-- <p>We believe in <strong>Simple</strong>, <strong>Creative</strong> &amp; <strong>Flexible</strong> Design Standards with a Retina &amp; Responsive Approach. Browse the amazing Features this template offers.</p> --}}

			<div class="clearfix" style="padding: 10px 0; background: url('{{asset('assets/images/world-map.png')}}') no-repeat center center;">
				<div class="col_half">
					<address class="nobottommargin">
						<abbr title="Headquarters" style="display: inline-block;margin-bottom: 7px;"><strong>Alamat:</strong></abbr><br>
						{{Frontend::contact()->address}}
					</address>
				</div>
				<div class="col_half col_last">
					<abbr title="Phone Number"><strong>Phone:</strong></abbr> {{Frontend::contact()->phone}}<br>
					<abbr title="Email Address"><strong>Email:</strong></abbr> {{Frontend::contact()->email}}<br>
					<abbr title="Whatsapp"><strong>Whatsapp:</strong></abbr> {{Frontend::contact()->whatsapp}}
				</div>
			</div>

			<a target="_blank" href="{{Frontend::contact()->facebook}}" class="social-icon si-small si-rounded topmargin-sm si-facebook">
				<i class="icon-facebook"></i>
				<i class="icon-facebook"></i>
			</a>

			<a target="_blank" href="{{Frontend::contact()->twitter}}" class="social-icon si-small si-rounded topmargin-sm si-twitter">
				<i class="icon-twitter"></i>
				<i class="icon-twitter"></i>
			</a>

			<a target="_blank" href="{{Frontend::contact()->google_plus}}" class="social-icon si-small si-rounded topmargin-sm si-gplus">
				<i class="icon-gplus"></i>
				<i class="icon-gplus"></i>
			</a>

			<a target="_blank" href="{{Frontend::contact()->instagram}}" class="social-icon si-small si-rounded topmargin-sm si-instagram">
				<i class="icon-instagram"></i>
				<i class="icon-instagram"></i>
			</a>

		</div>

	</div>

	<div class="col_one_fourth">

		{{-- <div class="widget widget_links clearfix">
			<h4>Situs Terkait</h4>

			<ul>
				<li><a href="https://kejaksaan.go.id/" target="_blank">Kejaksaan RI</a></li>
				<li><a href="http://www.kejari-gianyar.go.id/" target="_blank">Kejaksaan Negeri Gianyar</a></li>
				<li><a href="https://kejari-denpasar.go.id" target="_blank">Kejaksaan Negeri Denpasar</a></li>
				<li><a href="https://kejari-jembrana.go.id/" target="_blank">Kejaksaan Negeri Jembrana</a></li>
				<li><a href="https://kejari-tabanan.go.id/" target="_blank">Kejaksaan Negeri Tabanan</a></li>
			</ul>

		</div> --}}

	</div>

	<div class="col_one_fourth col_last">

		{{-- <div class="widget quick-contact-widget clearfix">

			<h4>Send Message</h4>

			<div class="quick-contact-form-result"></div>

			<form id="quick-contact-form" name="quick-contact-form" action="include/quickcontact.php" method="post" class="quick-contact-form nobottommargin">

				<div class="form-process"></div>

				<div class="input-group divcenter">
					<input type="text" class="required form-control input-block-level" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Full Name" />
				</div>
				<div class="input-group divcenter">
					<input type="text" class="required form-control email input-block-level" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Email Address" />
				</div>
				<textarea class="required form-control input-block-level short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="4" cols="30" placeholder="Message"></textarea>
				<input type="text" class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />
				<button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="btn btn-danger nomargin" value="submit">Send Email</button>

			</form>

		</div> --}}

	</div>

</div><!-- .footer-widgets-wrap end -->
