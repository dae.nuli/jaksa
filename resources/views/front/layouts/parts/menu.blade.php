<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
<ul>
	<li {{(Request::is('/') ? 'class=current' : '')}}><a href="{{url('/')}}"><div>Beranda Tp4d</div></a></li>
	<li {{(Request::is('permohonan') ? 'class=current' : '')}}><a href="{{url('permohonan')}}"><div>Permohonan Tp4d</div></a></li>
	<li {{(Request::is('pengaduan') ? 'class=current' : '')}}><a href="{{url('pengaduan')}}"><div>Pengaduan Tp4d</div></a></li>
	<li {{(Request::is('undang') || Request::is('undang/*') ? 'class=current' : '')}}><a href="{{url('undang')}}"><div>Undang Undang</div></a></li>
	{{-- <li {{(Request::is('mekanisme') ? 'class=current' : '')}}><a href="{{url('mekanisme')}}"><div>Mekanisme Tp4d</div></a></li> --}}
	<li {{(Request::is('kegiatan') ? 'class=current' : '')}}><a href="{{url('kegiatan')}}"><div>Kegiatan Tp4d</div></a></li>
	<li {{(Request::is('video') || Request::is('video/*') ? 'class=current' : '')}}><a href="{{url('video')}}"><div>Video</div></a></li>
	<li {{(Request::is('foto') || Request::is('foto/*') ? 'class=current' : '')}}><a href="{{url('foto')}}"><div>Foto</div></a></li>
</ul>

<!-- Top Search
============================================= -->
<div id="top-search">
	<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
	<form action="search.html" method="get">
		<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
	</form>
</div><!-- #top-search end -->