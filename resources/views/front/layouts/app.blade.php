<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/css/swiper.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}" type="text/css" />
  	@yield('head-script')

	<style type="text/css">

	</style>
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>{{$pageTitle or ''}} - TP4D KEJARI SUKOHARJO</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">

				<!-- Logo
				============================================= -->
				@include('front.layouts.parts.logo')

			</div>

			<div id="header-wrap">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="style-2 center">

					<div class="container clearfix">

						@include('front.layouts.parts.menu')

					</div>

				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->

		@yield('slider')

		@section('page-title')
			<!-- Page Title
			============================================= -->
			<section id="page-title">

				<div class="container clearfix">
					<h1>{{$pageTitle or "Title"}}</h1>
					{{-- <span>{{$pageDesc or "Desc"}}</span> --}}
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
						@if(isset($pageTitle))
						<li class="breadcrumb-item active" aria-current="page">{{$pageTitle}}</li>
						@endif
					</ol>
				</div>

			</section><!-- #page-title end -->
		@show
		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				{{-- <a href="#" class="button button-full button-purple center tright header-stick bottommargin-lg">
					<div class="container clearfix">
						Canvas comes with Unlimited Customizations &amp; Options. <strong>Check Out</strong> <i class="icon-caret-right" style="top:4px;"></i>
					</div>
				</a> --}}
				@yield('content-top')

				@yield('content-bottom')

				{{-- <div class="container clearfix">

					<div class="heading-block center">
						<h1>Recent Articles</h1>
						<span>We almost blog regularly about this &amp; that.</span>
					</div>

					<!-- Post Content
					============================================= -->
					<div class="postcontent nobottommargin clearfix">

						<!-- Posts
						============================================= -->
						<div id="posts" class="small-thumbs">

							<div class="entry clearfix">
								<div class="entry-image">
									<a href="{{asset('assets/images/portfolio/full/17.jpg')}}" data-lightbox="image"><img class="image_fade" src="{{asset('assets/images/blog/small/17.jpg')}}" alt="Standard Post with Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h2><a href="blog-single.html">This is a Standard post with a Preview Image</a></h2>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 10th February 2014</li>
										<li><a href="#"><i class="icon-user"></i> admin</a></li>
										<li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
									<div class="entry-content">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus voluptate id aperiam ea ipsum magni aut perspiciatis rem voluptatibus officia eos rerum deleniti quae nihil facilis repellat atque vitae voluptatem libero at eveniet veritatis ab facere.</p>
										<a href="blog-single.html"class="more-link">Read More</a>
									</div>
								</div>
							</div>


						</div><!-- #posts end -->

						<!-- Pagination
						============================================= -->
						<div class="row mb-3">
							<div class="col-12">
								<a href="#" class="btn btn-outline-secondary float-left">&larr; Older</a>
								<a href="#" class="btn btn-outline-dark float-right">Newer &rarr;</a>
							</div>
						</div>
						<!-- .pager end -->

					</div><!-- .postcontent end -->

					<!-- Sidebar
					============================================= -->
					<div class="sidebar nobottommargin col_last clearfix">

					</div><!-- .sidebar end -->

				</div> --}}

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				@include('front.layouts.parts.widget')
				<!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				@include('front.layouts.parts.copyright')

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{asset('assets/js/functions.js')}}"></script>
  	@yield('end-script')

</body>
</html>
