@extends('front.layouts.app')

@section('end-script')
<script type="text/javascript">

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tp4d-kejari-nunukan.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

@section('content-top')
<div class="container clearfix">

	<div class="single-post nobottommargin">

		<!-- Single Post
		============================================= -->
		<div class="entry clearfix">

			<!-- Entry Title
			============================================= -->
			<div class="entry-title">
				<h2>{{$detail->title}}</h2>
			</div><!-- .entry-title end -->

			<!-- Entry Meta
			============================================= -->
			<ul class="entry-meta clearfix">
				<li><i class="icon-calendar3"></i> {{date('d F Y', strtotime($detail->created_at))}}</li>
				<li><a href="#"><i class="icon-user"></i> {{$detail->admin->name}}</a></li>
				<li><i class="icon-folder-open"></i> <a href="#">UU</a></li>
				<li><a href="#"><i class="icon-line2-doc"></i></a></li>
			</ul><!-- .entry-meta end -->

			<!-- Entry Image
			============================================= -->
			{{-- <div class="entry-image bottommargin">
				<a href="#">
					<img src="{{asset('assets/images/blog/full/10.jpg')}}" alt="Blog Single">
				</a>
			</div><!-- .entry-image end --> --}}

			<!-- Entry Content
			============================================= -->
			<div class="entry-content notopmargin">
				<p>{!!$detail->content!!}</p>
				<a href="{{url('undang/view/'.$detail->id)}}"><i class="icon-download"></i> {{title_case($detail->title)}}</a></a>
			</div>
		</div><!-- .entry end -->

		<div class="line"></div>

		{{-- <h4>Related Video:</h4> --}}

		{{-- <div class="related-posts clearfix">

			<div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">

				@foreach($video as $row)
				<div class="entry clearfix">
					<div class="entry-image">
						<iframe width="560" height="315" src="{{'https://www.youtube.com/embed/'.$row->file}}" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="entry-title">
						<h2><a href="{{url('video/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h2>
					</div>
					<ul class="entry-meta clearfix">
						<li><i class="icon-calendar3"></i> {{date('d M Y', strtotime($row->created_at))}}</li>
						<li><a href="#"><i class="icon-film"></i></a></li>
					</ul>
					<div class="entry-content">
						<p>{{Str::words($row->content, 20)}}</p>
						<a href="{{url('video/'.$row->id)}}" class="more-link">Read More</a>
					</div>
				</div>
				@endforeach
			</div>

		</div> --}}

	</div>
	<div id="disqus_thread"></div>
</div>
@endsection