@extends('front.layouts.app')

@section('end-script')
<script type="text/javascript">

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tp4d-kejari-nunukan.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

@section('content-top')
				<div class="container clearfix">
					<div id="posts" class="small-thumbs">
						@foreach($undang as $row)
						<div class="entry clearfix">
							{{-- <div class="entry-image">
								<a href="{{asset('assets/images/blog/full/17.jpg')}}" data-lightbox="image"><img class="image_fade" src="{{asset('assets/images/blog/small/17.jpg')}}" alt="Standard Post with Image"></a>
							</div> --}}
							<div class="entry-c">
								<div class="entry-title">
									<h2><a href="{{url('undang/'.$row->id)}}">{{$row->title}}</a></h2>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> {{date('d F Y', strtotime($row->created_at))}}</li>
									<li><a href="#"><i class="icon-user"></i> {{$row->admin->name}}</a></li>
									<li><i class="icon-folder-open"></i> <a href="#">UU</a></li>
									{{-- <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li> --}}
									<li><a href="#"><i class="icon-line2-doc"></i></a></li>
								</ul>
								<div class="entry-content">
									<p>{{Str::words(strip_tags($row->content), 50)}}</p>
									<a href="{{url('undang/'.$row->id)}}" class="more-link">Read More</a>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					<div>
						{{$undang->links()}}
					</div>
					<div id="disqus_thread"></div>
				</div>
@endsection