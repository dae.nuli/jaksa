@extends('front.layouts.app')

@section('end-script')
<script type="text/javascript">

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tp4d-kejari-nunukan.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

@section('content-top')
<div class="container clearfix">
	<!-- Portfolio Single Image
	============================================= -->
	<div class="col_two_third portfolio-single-image nobottommargin">
		<iframe width="400" height="345" src="{{'https://www.youtube.com/embed/'.$detail->file}}" frameborder="0"></iframe>
		{{-- <a href="#"><img src="{{asset('store/foto/'.$detail->file)}}" alt=""></a> --}}
	</div><!-- .portfolio-single-image end -->

	<!-- Portfolio Single Content
	============================================= -->
	<div class="col_one_third portfolio-single-content col_last nobottommargin">

		<!-- Portfolio Single - Description
		============================================= -->
		<div class="fancy-title title-bottom-border">
			<h2>{{$detail->title}}</h2>
		</div>
		<small>By {{$detail->admin->name}} - {{date('d F Y', strtotime($detail->created_at))}}</small>
		<p>{{$detail->content}}</p>
		<!-- Portfolio Single - Description End -->

		<div class="line"></div>
	</div><!-- .portfolio-single-content end -->

	<div class="clear"></div>

	<div class="divider divider-center"><i class="icon-circle"></i></div>

	<!-- Related Portfolio Items
	============================================= -->
	<h4>Related Foto:</h4>

	<div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="false" data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">

		@foreach($video as $row)
		<div class="oc-item">
			<div class="iportfolio">
				<div class="portfolio-image">
					<a href="{{url('foto/'.$row->id)}}">
						<iframe width="420" height="345" src="{{'https://www.youtube.com/embed/'.$row->file}}" frameborder="0"></iframe>
						{{-- <img src="{{asset('store/foto/'.$row->file)}}" style="height: 180px; width: 100%; display: block;" alt="{{$row->title}}"> --}}
					</a>
					{{-- <div class="portfolio-overlay">
						<a href="{{asset('store/foto/'.$row->file)}}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
						<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
					</div> --}}
				</div>
				<div class="portfolio-desc">
					<h3><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 3)}}</a></h3>
				</div>
			</div>
		</div>
		@endforeach
	</div><!-- .portfolio-carousel end -->
	<div id="disqus_thread"></div>


</div>
@endsection