@extends('front.layouts.app')

@section('end-script')
<script type="text/javascript">

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tp4d-kejari-nunukan.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection

@section('content-top')
				<div class="container clearfix">
					<!-- Portfolio Items
					============================================= -->
					<div id="portfolio" class="portfolio grid-container portfolio-5 clearfix">

						@foreach($video as $row)
						<article class="portfolio-item">
							<div class="portfolio-image">
								<a href="{{url('video/'.$row->id)}}">
									<iframe width="420" height="345" src="{{'https://www.youtube.com/embed/'.$row->file}}" frameborder="0"></iframe>

									{{-- <img src="{{asset('store/foto/'.$row->file)}}" style="height: 180px; width: 100%; display: block;" alt="{{$row->title}}"> --}}
								</a>
								{{-- <div class="portfolio-overlay">
									<a href="{{asset('store/foto/'.$row->file)}}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div> --}}
							</div>
							<div class="portfolio-desc">
								<h3><a href="{{url('video/'.$row->id)}}">{{Str::words($row->title, 2)}}</a></h3>
							</div>
						</article>
						@endforeach

					</div><!-- #portfolio end -->
					<div>
						{{$video->links()}}
					</div>
					<div id="disqus_thread"></div>
				</div>
@endsection