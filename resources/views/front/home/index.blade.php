@extends('front.layouts.app')

@section('page-title')

@endsection

@section('slider')
<section id="slider" class="slider-element slider-parallax swiper_wrapper clearfix" data-autoplay="7000" data-speed="650" data-loop="true" style="height: 600px;">

	<div class="swiper-container swiper-parent">
		<div class="swiper-wrapper">
			@foreach($slider as $row)
			<div class="swiper-slide dark" style="background-image: url('{{asset("store/slider/".$row->file)}}');">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h2 data-caption-animate="fadeInUp">{{$row->title}}</h2>
						<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">{{$row->content}}</p>
					</div>
				</div>
			</div>
			@endforeach

			{{-- <div class="swiper-slide dark" style="background-image: url('{{asset("assets/images/slider/swiper/slider.png")}}');">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h2 data-caption-animate="fadeInUp">Buatlah Permohonan TP4D</h2>
						<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">Kami Siap Membantu Anda Dalam Mengatasi Permasalahan Anda.</p>
					</div>
				</div>
				
			</div> --}}
		</div>
		<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
		<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
		<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
	</div>

</section>
@endsection

@section('content-top')
<div class="container clearfix">
	<div class="col_one_third">
	    <a href="{{url('login')}}" class="button button-desc btn-block">
			<div>Login</div>
			<span>Halaman login TP4D</span>
		</a>
		<a href="{{url('permohonan')}}" class="button button-desc btn-block">
			<div>PERMOHONAN TP4D</div>
			<span>Data Permohonan Kegiatan TP4D</span>
		</a>
		<a href="{{url('kegiatan')}}" class="button button-desc btn-block">
			<div>KEGIATAN TP4D</div>
			<span>Data Hasil Kegiatan TP4D</span>
		</a>
		<a href="{{url('pengaduan')}}" class="button button-desc btn-block">
			<div>PENGADUAN TP4D</div>
			<span>Pengaduan Kegiatan TP4D</span>
		</a>

	</div>
	<div class="col_two_third col_last">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/Ux-JFRBAs7U?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</div>
@endsection

@section('content-bottom')
<div class="container clearfix">
	<div class="heading-block center">
		<h3>Kegiatan</h3>
	</div>
	<div class="col_full">
		<table class="table table-striped table-responsive">
			<thead>
				<tr>
				  	<th>No</th>
				  	<th>Pemohon</th>
				  	<th>Kegiatan</th>
				  	<th>Anggaran</th>
				  	<th>Mulai SPMK</th>
				  	<th>Berakhir SPMK</th>
				  	<th>Masa Pelaksanaan</th>
				  	<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>
				@foreach($kegiatan as $key => $row)
				<tr>
                    <th>{{$key + 1}}</th>
				  	<td>{{isset($row->user->name)?$row->user->name:'-'}}</td>
				  	{{-- <td>{{$row->instansi_pemohon}}</td> --}}
				  	<td>{{$row->nama_kegiatan}}</td>
				  	<td>Rp {{number_format($row->pagu_anggaran, 0, '', '.')}}</td>
				  	<td>{{$row->mulai_spmk}}</td>
				  	<td>{{$row->berakhir_spmk}}</td>
				  	<td>{{$row->masa_pelaksanaan}}</td>
				  	<td>{{$row->keterangan}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="container clearfix">
	<div class="heading-block center">
		<h3>Kerja Nyata</h3>
	</div>
	<div class="col_full">
        {{-- <img width="550" src="{{asset('assets/images/TP41.jpg')}}" class="img-thumbnail" alt="mengenal tp4">
        <img width="550" src="{{asset('assets/images/TP42.jpg')}}" class="img-thumbnail" alt="mekanisme kerja tp4">
        <img width="550" src="{{asset('assets/images/TP43.jpg')}}" class="img-thumbnail" alt="tp4 di mata mereka"> --}}
        <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">

		@foreach($kerja as $row)
			<div class="oc-item">
				<div class="iportfolio">
					<div class="portfolio-image">
						<a href="portfolio-single.html">
							<img src="{{asset('store/kerja/'.$row->file)}}" alt="{{$row->title}}">
						</a>
						<div class="portfolio-overlay">
							<a href="{{asset('store/kerja/'.$row->file)}}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
						</div>
					</div>
					<div class="portfolio-desc">
						<h3><a href="#">{{$row->title}}</a></h3>
						{{-- <span><a href="#">Media</a>, <a href="#">Icons</a></span> --}}
					</div>
				</div>
			</div>
		@endforeach

		</div>
	</div>
</div>
<div class="container clearfix">
	<div class="heading-block center">
		<h3>Foto</h3>
	</div>
	<div id="portfolio" class="portfolio grid-container clearfix">
		@foreach($foto as $row)
		<article class="portfolio-item">
			<div class="portfolio-image">
				<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
					<div class="flexslider">
						<div class="slider-wrap">
							@foreach($row->detail as $val)
							<div class="slide"><a href="{{url('foto/'.$row->id)}}"><img src="{{asset('store/foto/'.$val->file)}}" alt="{{$row->title}}"></a></div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="portfolio-overlay" data-lightbox="gallery">
					@foreach($row->detail as $key => $val)
						@if($key == 0)
						<a href="{{asset('store/foto/'.$val->file)}}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
						@else
						<a href="{{asset('store/foto/'.$val->file)}}" class="hidden" data-lightbox="gallery-item"></a>
						@endif
					@endforeach
					<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc">
				<h3><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h3>
			</div>
		</article>
		@endforeach
	</div>
</div>
@endsection