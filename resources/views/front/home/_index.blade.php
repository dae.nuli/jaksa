@extends('front.layouts.app')

@section('page-title')

@endsection

@section('slider')
<section id="slider" class="slider-element slider-parallax swiper_wrapper clearfix">

	<div class="swiper-container swiper-parent">
		<div class="swiper-wrapper">
			<div class="swiper-slide dark" style="background-image: url('{{asset("assets/images/slider/swiper/1.jpg")}}');">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h2 data-caption-animate="fadeInUp">Welcome to Canvas</h2>
						<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p>
					</div>
				</div>
			</div>
			<div class="swiper-slide dark">
				<div class="container clearfix">
					<div class="slider-caption slider-caption-center">
						<h2 data-caption-animate="fadeInUp">Beautifully Flexible</h2>
						<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionality that can be adapted to any screen size.</p>
					</div>
				</div>
				<div class="video-wrap">
					<video poster="{{asset('assets/images/videos/explore.jpg')}}" preload="auto" loop autoplay muted>
						<source src='{{asset('assets/images/videos/explore.mp4')}}' type='video/mp4' />
						<source src='{{asset('assets/images/videos/explore.webm')}}' type='video/webm' />
					</video>
					<div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
				</div>
			</div>
			<div class="swiper-slide" style="background-image: url('{{asset('assets/images/slider/swiper/3.jpg')}}'); background-position: center top;">
				<div class="container clearfix">
					<div class="slider-caption">
						<h2 data-caption-animate="fadeInUp">Great Performance</h2>
						<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">You'll be surprised to see the Final Results of your Creation &amp; would crave for more.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
		<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
		<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
	</div>

</section>
@endsection

@section('content-top')
<div class="container clearfix">
	<div class="col_one_third">
		<a href="{{url('kegiatan')}}" class="button button-desc btn-block">
			<div>KEGIATAN TP4D</div>
			<span>Data Permohonan Kegiatan TP4D</span>
		</a>
		<a href="http://google.com" class="button button-desc btn-block">
			<div>KEJATI BALI</div>
			<span>Web Kejati Bali</span>
		</a>
		<a href="{{url('login')}}" class="button button-desc btn-block">
			<div>Login</div>
		</a>
	</div>
	<div class="col_two_third col_last">
		Home
	</div>
</div>
@endsection

@section('content-bottom')
<div class="container clearfix">
	<div class="heading-block center">
		<h3>Foto</h3>
		{{-- <span>We have worked on some Awesome Projects that are worth boasting of.</span> --}}
	</div>
{{-- 
	<div id="oc-portfolio" class="owl-carousel portfolio-carousel portfolio-nomargin carousel-widget" data-margin="1" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">
	@foreach($foto as $row)
		<div class="oc-item">
			<div class="iportfolio">
				<div class="portfolio-image">
					<a href="{{url('foto/'.$row->id)}}">
						<img src="{{asset('store/foto/'.$row->file)}}" style="height: 180px; width: 100%; display: block;" alt="{{$row->title}}">
					</a>
					<div class="portfolio-overlay">
						<a href="{{asset('store/foto/'.$row->file)}}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
						<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
					</div>
				</div>
				<div class="portfolio-desc">
					<h3><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 3)}}</a></h3>
				</div>
			</div>
		</div>
	@endforeach

	</div> --}}
	<div id="portfolio" class="portfolio grid-container clearfix">
		@foreach($foto as $row)
		<article class="portfolio-item">
			<div class="portfolio-image">
				<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
					<div class="flexslider">
						<div class="slider-wrap">
							@foreach($row->detail as $val)
							<div class="slide"><a href="{{url('foto/'.$row->id)}}"><img src="{{asset('store/foto/'.$val->file)}}" alt="{{$row->title}}"></a></div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="portfolio-overlay" data-lightbox="gallery">
					@foreach($row->detail as $key => $val)
						@if($key == 0)
						<a href="{{asset('store/foto/'.$val->file)}}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
						@else
						<a href="{{asset('store/foto/'.$val->file)}}" class="hidden" data-lightbox="gallery-item"></a>
						@endif
					@endforeach
					<a href="{{url('foto/'.$row->id)}}" class="right-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc">
				<h3><a href="{{url('foto/'.$row->id)}}">{{Str::words($row->title, 4)}}</a></h3>
			</div>
		</article>
		@endforeach
	</div>
</div>
@endsection