@extends('front.layouts.app')

{{-- @section('head-script')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
	<style type="text/css">
		.uploaded_image {
		    margin-bottom: 0 !important;
		}
	</style>
@endsection
 --}}
@section('end-script')
	{{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
	<!-- Select2 -->
	<script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
	<script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
	<script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.priceformat.min.js') }}"></script>
	<script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
	{{-- <script src="{{ asset('assets/js/jquery.multifile.js') }}"></script> --}}
	<script type="text/javascript">
	$.validate({
	    // form : 'form',
	    onSuccess : function() {
	      waiting();
	    }
	});
	$('.number').priceFormat({
	  prefix: '',
	  thousandsSeparator: '.',
	  clearPrefix:true,
	  centsLimit: 0
	});
	// $('#multifile').multifile();
	</script>
@endsection

@section('content-top')
<div class="container clearfix">
	<div class="row justify-content-center">
		<div class="col-md-10">
			@if(Session::has('status'))
			<div class="alert alert-success">
			  	<i class="icon-gift"></i>{{Session::get('status')}}
			</div>
			@endif
			
			@if ($errors->any())
			<div class="style-msg2 errormsg">
					<div class="msgtitle">Fix the Following Errors:</div>
					<div class="sb-msg">
						<ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
						</ul>
					</div>
				</div>
			@endif
		</div>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-10">
			<form action="{{url('permohonan')}}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group row">
					<label for="pemohon" class="col-md-4 col-form-label text-md-right">Instansi Pemohon</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="pemohon" value="{{old('instansi_pemohon')}}" data-validation="required" name="instansi_pemohon">
					</div>
				</div>
				<div class="form-group row">
					<label for="kegiatan" class="col-md-4 col-form-label text-md-right">Nama Kegiatan</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="kegiatan" value="{{old('nama_kegiatan')}}" data-validation="required" name="nama_kegiatan">
					</div>
				</div>
				<div class="form-group row">
					<label for="jenis_pekerjaan" class="col-md-4 col-form-label text-md-right">Jenis Pekerjaan/Proyek</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="jenis_pekerjaan" value="{{old('jenis_pekerjaan')}}" data-validation="required" name="jenis_pekerjaan">
					</div>
				</div>
				{{-- <div class="form-group row">
					<label for="pagu" class="col-md-4 col-form-label text-md-right">Pagu Anggaran</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="pagu" value="{{old('pagu_anggaran')}}" data-validation="required" name="pagu_anggaran">
					</div>
				</div>
				<div class="form-group row">
					<label for="sumber_dana" class="col-md-4 col-form-label text-md-right">Sumber Dana</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="sumber_dana" value="{{old('sumber_dana')}}" data-validation="required" name="sumber_dana">
					</div>
				</div> --}}
				<div class="form-group row">
					<label for="tahapan_proyek" class="col-md-4 col-form-label text-md-right">Tahapan Proyek Pekerjaan</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="tahapan_proyek" value="{{old('tahapan_proyek')}}" data-validation="required" name="tahapan_proyek">
					</div>
				</div>
				<div class="form-group row">
					<label for="pengawalan" class="col-md-4 col-form-label text-md-right">Bentuk Pengawalan & Pengamanan</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="pengawalan" value="{{old('bentuk_pengawalan')}}" data-validation="required" name="bentuk_pengawalan">
					</div>
				</div>
				<div class="form-group row">
					<label for="nama" class="col-md-4 col-form-label text-md-right">Nama Penanggung Jawab</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="nama" value="{{old('nama_penanggungjawab')}}" data-validation="required" name="nama_penanggungjawab">
					</div>
				</div>
				<div class="form-group row">
					<label for="persentase_permasalahan" class="col-md-4 col-form-label text-md-right">Persentase Permasalahan</label>
					<div class="col-md-7">
						<input type="number" class="form-control" id="persentase_permasalahan" value="{{old('persentase_permasalahan')}}" data-validation="required" name="persentase_permasalahan">
					</div>
				</div>
				<div class="form-group row">
					<label for="telp" class="col-md-4 col-form-label text-md-right">Telp</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="telp" value="{{old('phone')}}" data-validation="required" name="phone">
					</div>
				</div>
				<div class="form-group row">
					<label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
					<div class="col-md-7">
						<input type="email" class="form-control" id="email" value="{{old('email')}}" data-validation="required" name="email">
					</div>
				</div>
				<div class="form-group row">
					<label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
					<div class="col-md-7">
						<input type="password" class="form-control" id="password" data-validation="required" name="password">
					</div>
				</div>
{{-- 				<div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right">File</label>
					<div class="col-md-7">
						<input type="file" class="form-control multifile" id="multifile" name="attach[]">
					</div>
				</div> --}}
				<div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right"></label>
					<div class="col-md-7">
						{!! captcha_img('flat') !!}
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right">Captcha</label>
					<div class="col-md-7">
						<input type="text" class="form-control"  name="captcha">
					{{-- {!! app('captcha')->display() !!} --}}
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-7 offset-md-4">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection