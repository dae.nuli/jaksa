@extends('layouts.app')

@section('head-script')
<style type="text/css">
    .carousel-inner {
        background: grey;
    }
    .carousel-inner h2 {
        font-size: 3rem;
        letter-spacing: 2px;
        text-transform: uppercase;
        font-weight: 700;
        line-height: 1;
        margin-bottom: 0;
    }
</style>
@endsection

@section('end-script')
<script type="text/javascript">
    $('.carousel').carousel();
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center clearfix">
        <div class="col-md-10">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($slider as $i => $row)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{($i == 0)?'active' : ''}}"></li>
                    {{-- <li data-target="#carouselExampleIndicators" data-slide-to="1"></li> --}}
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach($slider as $i => $row)
                    <div class="carousel-item {{($i == 0)?'active' : ''}}">
                        <img class="d-block w-100" src='{{asset("store/slider/".$row->file)}}' alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2>{{$row->title}}</h2>
                            <p>{{$row->content}}</p>
                        </div>
                    </div>
                    @endforeach
                    {{-- <div class="carousel-item">
                        <img class="d-block w-100" src='{{asset("assets/images/slider/swiper/slider.png")}}' alt="Second slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2>Buatlah Permohonan TP4D</h2>
                            <p>Kami Siap Membantu Anda Dalam Mengatasi Permasalahan Anda.</p>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center clearfix">
        <div class="col-md-10">
            <iframe width="100%" height="415" src="https://www.youtube.com/embed/Ux-JFRBAs7U?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
    <br>
    <div class="row justify-content-center clearfix">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Kerja Nyata</div>
                <div class="card-body">
                    @foreach($kerja as $i => $row)
                        <a href="{{url('client/kerja/'.$row->id)}}" target="_blank"><img width="200" src="{{asset('store/kerja/'.$row->file)}}" class="img-thumbnail rounded float-left" alt="{{$row->title}}"></a>
                    @endforeach
                    {{-- <img width="430" src="{{asset('assets/images/TP41.jpg')}}" class="img-thumbnail clearfix" alt="mengenal tp4">
                    <img width="430" src="{{asset('assets/images/TP42.jpg')}}" class="img-thumbnail clearfix" alt="mekanisme kerja tp4">
                    <img width="430" src="{{asset('assets/images/TP43.jpg')}}" class="img-thumbnail clearfix" alt="tp4 di mata mereka"> --}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-10">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Kegiatan</div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Pemohon</th>
                                <th>Kegiatan</th>
                                <th>Anggaran</th>
                                <th>Mulai SPMK</th>
                                <th>Berakhir SPMK</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kegiatan as $key => $row)
                            <tr>
                                <th>{{$key + 1}}</th>
                                <td>{{$row->user->name}}</td>
                                <td>{{$row->nama_kegiatan}}</td>
                                <td>Rp {{number_format($row->pagu_anggaran, 0, '', '.')}}</td>
                                <td>{{$row->mulai_spmk}}</td>
                                <td>{{$row->berakhir_spmk}}</td>
                                <td>{{$row->keterangan}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Foto</div>

                <div class="card-body">
                    @foreach($foto as $row)
                        <a href="{{url('foto/'.$row->id)}}" target="_blank"><img width="200" src="{{asset('store/foto/'.$row->oneFoto()->file)}}" class="img-thumbnail rounded float-left" alt="{{$row->title}}"></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
