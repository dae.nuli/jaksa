<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('kegiatan', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('pemohon');
        //     $table->text('kegiatan');
        //     $table->integer('anggaran');
        //     $table->string('keterangan');
        //     $table->timestamps();
        // });

        Schema::create('kegiatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            // $table->string('instansi_pemohon')->nullable();
            $table->string('nama_kegiatan');
            $table->string('jenis_pekerjaan');
            $table->bigInteger('pagu_anggaran')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->date('jadwal_sosialisasi')->nullable();

            $table->string('nomor_surat_perintah')->nullable();
            $table->string('tim_pelaksana')->nullable();
            $table->string('tahapan_proyek')->nullable();
            $table->string('tahapan_pengawalan')->nullable();
            $table->date('mulai_spmk')->nullable();
            $table->date('berakhir_spmk')->nullable();
            $table->string('masa_pelaksanaan')->nullable();
            $table->date('jadwal_monitoring')->nullable();
            
            $table->string('bentuk_pengawalan')->nullable();
            $table->string('nama_penanggungjawab')->nullable();
            $table->integer('persentase_permasalahan')->nullable();
            $table->string('phone', 20);
            $table->string('penyerapan_anggaran')->nullable();
            $table->integer('status')->default(0);
            $table->string('keterangan')->nullable();
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kegiatan');
    }
}
