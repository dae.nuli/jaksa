<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengaduanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('nama_pengadu');
            $table->string('nama_kegiatan');
            // $table->bigInteger('nilai_kegiatan');
            $table->text('permasalahan')->nullable();
            $table->string('penanggungjawab')->nullable();
            $table->string('phone', 20);
            // $table->string('email', 50);
            // $table->string('file');
            $table->string('keterangan')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduan');
    }
}
