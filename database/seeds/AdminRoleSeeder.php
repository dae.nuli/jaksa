<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\Models\Role;
use App\Models\AdminRole;

class AdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$ad = Admin::find(1);
    	$rl = Role::where('slug', 'administrator')->first();
    	AdminRole::truncate();
        AdminRole::insert([
            'admin_id' => $ad->id,
            'role_id' => $rl->id,
        ]);
    }
}
