<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::truncate();
        Role::insert([
        	[
                'slug' => 'administrator',
	        	'name' => 'Administrator',
				'description' => 'Super User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'slug' => 'staff',
	        	'name' => 'Staff',
				'description' => 'Staff User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
