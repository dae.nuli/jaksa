<?php

use Illuminate\Database\Seeder;
use App\Models\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Contact::truncate();
        Contact::insert([
        	'phone' => '081915804771',
			'email' => 'email@example.com',
			'web' => 'http://hello.com',
			'address' => 'Jl Kaliurang KM 14',
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}



