<?php

use Illuminate\Database\Seeder;
use App\Models\RolePermission;
use App\Models\Permission;
use App\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	RolePermission::truncate();

    	$pm = Permission::all();
    	$rl = Role::where('slug', 'administrator')->first();

    	foreach ($pm as $key => $value) {
	        $rp = new RolePermission;
	        $rp->role_id = $rl->id;
	        $rp->permission_id = $value->id;
	        $rp->save();
    	}
    }
}
