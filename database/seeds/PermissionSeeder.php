<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Permission::truncate();
        Permission::insert([
        	[
	        	'name' => 'foto_index',
				'description' => 'Menu Index Foto'
        	],
        	[
	        	'name' => 'foto_create',
				'description' => 'Menu Create Foto'
        	],
        	[
	        	'name' => 'foto_edit',
				'description' => 'Menu Edit Foto'
        	],
            [
                'name' => 'foto_detail',
                'description' => 'Menu Detail Foto'
            ],
            [
                'name' => 'foto_delete',
                'description' => 'Menu Delete Foto'
            ],
        	
        	[
	        	'name' => 'video_index',
				'description' => 'Menu Index Video'
        	],
        	[
	        	'name' => 'video_create',
				'description' => 'Menu Create Video'
        	],
        	[
	        	'name' => 'video_edit',
				'description' => 'Menu Edit Video'
        	],
        	[
	        	'name' => 'video_detail',
				'description' => 'Menu Detail Video'
        	],
            [
                'name' => 'video_delete',
                'description' => 'Menu Delete Video'
            ],
        	
        	[
	        	'name' => 'kegiatan_index',
				'description' => 'Menu Index Kegiatan'
        	],
        	[
	        	'name' => 'kegiatan_create',
				'description' => 'Menu Create Kegiatan'
        	],
        	[
	        	'name' => 'kegiatan_edit',
				'description' => 'Menu Edit Kegiatan'
        	],
        	[
	        	'name' => 'kegiatan_detail',
				'description' => 'Menu Detail Kegiatan'
        	],
            [
                'name' => 'kegiatan_delete',
                'description' => 'Menu Delete Kegiatan'
            ],
        	
        	[
	        	'name' => 'pengaduan_index',
				'description' => 'Menu Index Pengaduan'
        	],
        	[
	        	'name' => 'pengaduan_create',
				'description' => 'Menu Create Pengaduan'
        	],
        	[
	        	'name' => 'pengaduan_edit',
				'description' => 'Menu Edit Pengaduan'
        	],
        	[
	        	'name' => 'pengaduan_detail',
				'description' => 'Menu Detail Pengaduan'
        	],
            [
                'name' => 'pengaduan_delete',
                'description' => 'Menu Delete Pengaduan'
            ],
        	
        	[
	        	'name' => 'undang_index',
				'description' => 'Menu Index UU'
        	],
        	[
	        	'name' => 'undang_create',
				'description' => 'Menu Create UU'
        	],
        	[
	        	'name' => 'undang_edit',
				'description' => 'Menu Edit UU'
        	],
        	[
	        	'name' => 'undang_detail',
				'description' => 'Menu Detail UU'
        	],
            [
                'name' => 'undang_delete',
                'description' => 'Menu Delete UU'
            ],
            
            [
                'name' => 'user_index',
                'description' => 'Menu Index User'
            ],
            [
                'name' => 'user_create',
                'description' => 'Menu Create User'
            ],
            [
                'name' => 'user_edit',
                'description' => 'Menu Edit User'
            ],
            [
                'name' => 'user_delete',
                'description' => 'Menu Delete User'
            ],
            
            [
                'name' => 'visitor_index',
                'description' => 'Menu Index Visitor'
            ],
            
            [
                'name' => 'admin_index',
                'description' => 'Menu Index Admin'
            ],
            [
                'name' => 'admin_create',
                'description' => 'Menu Create Admin'
            ],
            [
                'name' => 'admin_edit',
                'description' => 'Menu Edit Admin'
            ],
            [
                'name' => 'admin_delete',
                'description' => 'Menu Delete Admin'
            ],
            
            [
                'name' => 'role_index',
                'description' => 'Menu Index Role'
            ],
            [
                'name' => 'role_create',
                'description' => 'Menu Create Role'
            ],
            [
                'name' => 'role_edit',
                'description' => 'Menu Edit Role'
            ],
            [
                'name' => 'role_delete',
                'description' => 'Menu Delete Role'
            ],
            
            [
                'name' => 'permission_index',
                'description' => 'Menu Index Permission'
            ],
            
            [
                'name' => 'contact_index',
                'description' => 'Menu Index Contact'
            ],

            [
                'name' => 'slider_index',
                'description' => 'Menu Index Slider'
            ],
            [
                'name' => 'slider_create',
                'description' => 'Menu Create Slider'
            ],
            [
                'name' => 'slider_edit',
                'description' => 'Menu Edit Slider'
            ],
            [
                'name' => 'slider_detail',
                'description' => 'Menu Detail Slider'
            ],
            [
                'name' => 'slider_delete',
                'description' => 'Menu Delete Slider'
            ],

            [
                'name' => 'kerja_index',
                'description' => 'Menu Index Kerja'
            ],
            [
                'name' => 'kerja_create',
                'description' => 'Menu Create Kerja'
            ],
            [
                'name' => 'kerja_edit',
                'description' => 'Menu Edit Kerja'
            ],
            [
                'name' => 'kerja_detail',
                'description' => 'Menu Detail Kerja'
            ],
            [
                'name' => 'kerja_delete',
                'description' => 'Menu Delete Kerja'
            ],
        ]);
    }
}
